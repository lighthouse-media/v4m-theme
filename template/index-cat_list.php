
<?php

    $terms = get_terms( 'product_cat', array(
        'hide_empty' => false,
        'parent' => 0,
        'exclude'=>15
    ) );
    
?>

<div class="cat-list-block-wrapper">
    <div class="cat-list-wrapper container">
        <div class="row">
            <div class="col-12">
                <h2>Kategorijas</h2>
            </div>
            <?php

                foreach($terms as $term) {
                    get_template_part( 'template/cat', 'grid_item', $term );
                }

            ?>
        </div>
    </div>
</div>
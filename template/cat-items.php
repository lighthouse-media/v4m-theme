<?php

if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */

    $cat_slug = get_query_var('product_cat');
    $cat = get_term_by('slug', $cat_slug, 'product_cat');
    $desc = get_term_meta($cat->term_id, 'category_description', true);

    woocommerce_output_all_notices();
    if ($desc) echo '<h2>Produkti</h2>';
	echo '<div class="container cat-items">';
    echo '<div class="row">';
        get_template_part( 'template/prod', 'ordering' );
    echo '</div>';
    echo '<div class="row">';

	if ( wc_get_loop_prop( 'total' ) ) {
        /* global $wp_query;
        var_dump($wp_query->query_vars); */
		while ( have_posts() ) {
			the_post();

			wc_get_template_part( 'content', 'product' );
		}
	}
	echo '</div></div>';

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}



?>



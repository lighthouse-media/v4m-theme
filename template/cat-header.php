<?php
    $cat_slug = get_query_var('product_cat');
    $cat = get_term_by('slug', $cat_slug, 'product_cat');
    $desc = get_term_meta($cat->term_id, 'category_description', true);
    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true ); 
    $image = wp_get_attachment_url( $thumbnail_id );
?>
<div class="container">
        <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
            <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
        <?php endif; ?>
        <?php if ($desc) : ?>
            <div class="category-description">
                
                <div class="desc-txt"><?php echo apply_filters('the_content',$desc); ?></div>

                <?php if ($image) : ?>
                    <div class="category-image" style="background-image:url(<?= $image; ?>)">
                <?php endif; ?>

                
            </div>
        <?php endif; ?>
</div>
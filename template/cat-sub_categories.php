<?php
    $cat_slug = get_query_var('product_cat');
    $cat = get_term_by('slug', $cat_slug, 'product_cat');
    //var_dump( $cat);

    $args = array(
        'hide_empty' => false,
        'parent' => $cat->term_id,
        'exclude'=>15
    );
    $child_terms = get_terms('product_cat', $args);
    if ( count($child_terms) ) :
?>
<div class="cat-list-wrapper container">
    <div class="row">
        <?php

            foreach($child_terms as $term) {
                get_template_part( 'template/cat', 'grid_item', $term );
            }

        ?>
    </div>
</div>

<?php endif; ?>
<?php

    if (isset($_GET['orderby'])) {
        $sel = $_GET['orderby'];
    } else {
        $sel = 'menu_order';
    }

?>

<form class="woocommerce-ordering" method="get" >
    <div class="orderby">
        <select name="orderby"  aria-label="Shop order">
            <option value="menu_order"<?= $sel=='menu_order' ? ' selected' : ''?>>Pēc noklusējuma</option>
            <option value="title"<?= $sel=='title' ? ' selected' : ''?>>Nosaukums no A līdz Z</option>
            <option value="price"<?= $sel=='price' ? ' selected' : ''?>>Zemāka cena</option>
            <option value="price-desc"<?= $sel=='price-desc' ? ' selected' : ''?>>Augstāka cena</option>
        </select>
    </div>
	<input type="hidden" name="paged" value="1">
</form>
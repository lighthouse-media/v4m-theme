<?php

    $args = wp_parse_args( $args );

    $thumbnail_id = get_term_meta( $args['term_id'], 'thumbnail_id', true );
    $image = wp_get_attachment_url( $thumbnail_id );
    $term_link = get_term_link( $args['term_id'], 'product_cat');

    $smaller_img = get_term_meta( $args['term_id'], 'make_icon_smaller', true );

?>
<div class="cat-grid-item-wrapper col-xs-12 col-sm-6 col-md-4 col-lg-3">
    <div class="cat-grid-item <?= $args['slug']; ?><?= $smaller_img ? ' smaler-img' : ''; ?>">

        <div class="cat-image" style="background-image:url(<?= $image; ?>)"></div>
        <h4><?= $args['name']; ?></h4>
        <a class="overlay-link" href="<?= $term_link; ?>"></a>
    </div>
</div>
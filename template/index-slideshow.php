

<div class="slideshow-block-wrapper">
<?php

    $args = array(
        "post_type" => 'slides',
        "post_status" => 'publish'
    );

    $slides = new WP_Query($args);
    $counter = 0;

    $slide_links = '<div class="slide-links">';

    if ($slides->have_posts()) :
        echo '<div class="slideshow-wrapper container"><div class="slides">';
        while ($slides->have_posts()) :
            $slides->the_post();
            $thumb_horizontal_id = get_post_meta($post->ID, 'horizontal_image', true);
            //var_dump($thumb_horizontal_id);
            $thumb_horizontal = wp_get_attachment_image_src($thumb_horizontal_id, 'full');
            //var_dump($thumb_horizontal);
            $thumb_vertical_id = get_post_meta($post->ID, 'vertical_image', true);
            $thumb_vertical = wp_get_attachment_image_src($thumb_vertical_id, 'full');
            
            $post_link = get_post_meta( $post->ID, 'link', true );


            echo sprintf('<div class="slide%s"%s><div class="slide-img" style="background-image:url(%s)" data-horizontal="%s" data-vertical="%s">%s</div></div>',
                $counter ? '' : ' active',
                !$counter ? ' style="display:flex; opacity:1"' : '',
                $thumb_horizontal[0],
                $thumb_horizontal[0],
                $thumb_vertical[0],
                $post_link ? '<a href="' . $post_link . '"></a>' : ''
            );

            $slide_links .= sprintf('<div class="slide-link%s" data-index="%s"></div>',
                $counter ? '' : ' active',
                $counter
            );

            $counter++;
        endwhile;
        echo '</div></div>';
    endif;

    $slide_links .= '</div>';

    wp_reset_query();

?>

    <?php echo $slide_links; ?>

    <div class="slideshow-icons container">
        <div class="row">
            <div class="col-6 col-md-3 offset-md-3 offset-0 slider-icons">
                <div class="the-icon delivery"></div>
                <span>Ērta piegāde 3-5 darba dienās</span>
            </div>
            <div class="col-6 col-md-3 slider-icons">
                <div class="the-icon quality"></div>
                <span>Tieši no ražotāja</span>    
            </div>
        </div>
    </div>
</div>
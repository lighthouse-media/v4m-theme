<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( ); ?>

<?php
$product = wc_get_product( $post->ID );
$img = $product->get_image();
$title = $product->get_name();
$desc = $product->get_short_description();
$type = $product->get_type();
$leftover = $product->get_stock_quantity();
$price = $product->get_price();
$original_price = $product->get_sale_price() ? $product->get_regular_price() : 0;
$link = get_permalink( $post->ID );
$id = $post->ID;
$is_popular_product = get_post_meta($post->ID, 'is_popular_product', true);
$non_gmo = get_post_meta($post->ID, 'non_gmo', true);

$weight = get_post_meta( $id, 'netto_weight', true );
if ( !$weight ) {
    $weight = $product->get_weight();
}

$atts = $product->get_attributes();
$product_properties = $atts && array_key_exists("pa_property", $atts) ? $atts["pa_property"]['options'] : false;
$product_prop_processed = [];
if ($product_properties && count($product_properties)) {
    foreach($product_properties as $prop_id) {
        $the_prop = [];
        $the_term = get_term( $prop_id, 'pa_property' )->name;
        $the_icon_id = get_term_meta( $prop_id, 'icon', true );
        $the_icon = wp_get_attachment_url($the_icon_id);
        $the_prop['name'] = $the_term;
        $the_prop['icon'] = $the_icon;
        array_push($product_prop_processed, $the_prop);
    }
}

if ($type == 'variable') {
    $fist_var_id = $product->get_children()[0];
    $first_var = wc_get_product($fist_var_id);
    $price = $first_var->get_price();
    $leftover = $first_var->get_stock_quantity();
    $id = $fist_var_id;
}

$cart = WC()->cart;
$cart_amounts = $cart->get_cart_item_quantities();
$amount = array_key_exists($id, $cart_amounts) ?  $cart_amounts[$id] : 0;


?>



		<?php while ( have_posts() ) : ?>
            <?php the_post(); ?>
            <div class="container">
                <div class="row main-product-info" data-prodid="<?= $post->ID; ?>">
            
                    <div class="col-md-6 col-12">
                        <div class="prod-img-wrapper">
                            <?php woocommerce_show_product_images(); ?>
                            <?php if ($original_price){
                                $add_class = $is_popular_product ? ' is-pop' : '';
                                echo '<div class="has-discount' . $add_class . '">-' . number_format(((1-$price/$original_price)*100), 0) . '%</div>';
                            } ?>
                            <?php if ($is_popular_product) {

                                echo '<div class="popular-product"></div>';
                            } ?>
                            <?php
                                if ($non_gmo) {
                                    echo sprintf('<div class="non-gmo%s%s"></div>',
                                        $is_popular_product && $original_price ? ' two-front' : '',
                                        $is_popular_product || $original_price ? ' one-front' : ''
                                );
                                }
                            ?>

                        </div>
                        <?php
                        $fieldnames = ['how_to_use', 'consists'];
                        $fieldvals = [];
                        foreach ($fieldnames as $fieldname) {
                            $field = get_post_meta( $post->ID, $fieldname, true );
                            if ($field) $fieldvals[$fieldname] = $field;
                        }
                        if (count($fieldvals)) : ?>
                            <?php $counter = 0; ?>
                            <div class="overview-wrapper">
                                <h2>Produkta raksturojums</h2>
                                <div class="accordion">
                                    <?php foreach ($fieldnames as $fieldname) { 
                                        if ($fieldvals[$fieldname]) {
                                    ?>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading_<?= $fieldname; ?>">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse_<?= $fieldname; ?>" aria-expanded="false" aria-controls="collapse_<?= $fieldname; ?>">
                                                    <?php
                                                        if ($fieldname == 'how_to_use') echo 'Lietošana';
                                                        if ($fieldname == 'consists') echo 'Sastāvs';
                                                    ?>
                                                </button>
                                            </h2>
                                            <div id="collapse_<?= $fieldname; ?>" class="accordion-collapse collapse" aria-labelledby="heading_<?= $fieldname; ?>" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <?php
                                                        $field = apply_filters( 'the_content', $fieldvals[$fieldname] );
                                                        echo $field;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                        $counter++;
                                        }
                                    } ?>
                                </div>
                            </div>
                        <?php endif; ?>
                
                            
                        
                    </div>

                    <div class="col-md-6 col-12">
                        <div class="order-setup">
                            <div class="single-product title_and_price">
                                <h1><?= $title;?></h1>
                                <div class="price-blk">
                                    <div class="price">
                                        <?php if ($original_price) echo "<div class=\"original-price\">€$original_price</div>"; ?>
                                        <span class="actual-price<?php if ($original_price) echo ' has-discount'; ?>">€<?= number_format($price, 2); ?></span>
                                    </div>
                                    <div class="pvn">Cena bez PVN: <?= number_format($price/121*100, 2); ?> €<br> PVN 21%: <?= number_format($price/121*21, 2); ?> €</div>
                                    <?php if ($weight) : ?>
                                        <div class="per-kg">Cena par kg: <?= number_format($price/$weight, 2); ?> €</div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if (false) : ?>
                                <p class="pack-size"><b>Apjoms:</b> <?= $product->get_attribute( 'unit' ); ?></p>
                            <?php endif; ?>
                            <?php if ($type == 'variable') : ?>
                                <div class="pack-size-blk">
                                    <p class="pack-size"><b>Apjoms:</b></p>
                                    <select name="unit" class="unit">
                                        <?php
                                            $variation_ids = $product->get_children();
                                            foreach( $variation_ids as $var_id ) {
                                                $var = wc_get_product($var_id);
                                                $unit = $var->get_attribute( 'unit' );
                                                $price = $var->get_price();
                                                $stock = $var->get_stock_quantity();
                                                echo '<option data-id="' . $var_id . '" data-stockinfo="' . show_stock($stock) . '" data-stock="' . $stock . '" data-price="' . $price . '" value=' . $unit . ">" . get_display_unit($var, $product) . "</option>" ;
                                            
                                            }
                                        ?>
                                    </select>
                        
                                </div>
                            <?php endif; ?>

                            <?php if ( $leftover ) : ?>
                                <div class="amount-and-add<?php if (!$leftover) echo ' hidden' ?>">
                                    <div class="amount<?php if (!$leftover) echo ' hidden' ?>">
                                        <div class="label">Daudzums: </div>
                                        <div class="amount-controls product-amount">
                                            <div class="ctrl-btn" data-ctrl="-1">-</div>
                                            <div class="amount"><input type="number" value="1" min="0" max="<?= $amount ? $leftover - $amount : $leftover; ?>" ></div>
                                            <div class="ctrl-btn" data-ctrl="1">+</div>
                                        </div>
                                    </div>

                                    <?php
                                        echo '<div class="in-basket">';
                                        if ($amount) {
                                            echo sprintf('Grozā %s', $amount );
                                        }
                                        echo '</div>';
                                    ?>
                                    
                                        <div
                                            class="add-to-basket"
                                            data-instock="Ielikt grozā"
                                            data-link="<?= $link; ?>"
                                            data-notinstock="Aplukot produktu"
                                            data-productid=""
                                            data-id="<?= $id; ?>"
                                            data-amount="1"
                                            data-allow="<?= $leftover ? 1 : 0 ?>"
                                        >Ielikt grozā</div>
                                
                                </div>
                            <?php endif; ?>
                            <div class="leftover <?= $leftover ? 'in-stock' : 'not-in-stock'; ?>">
                                <?php echo show_stock($leftover); ?>
                            </div>
                            <div class="delivery-info">
                                <div class="delivery-icon"></div>
                                <div class="delivery-data">
                                    <b>Ērta piegāde:</b> <span class="<?= $leftover ? 'in-stock' : 'not-in-stock'; ?>">3-5 darba dienu laikā</span></br>
                                    Ar piegādes cenām un noteikumiem Jūs varat iepazīties <a href="<?php echo get_home_url(); ?>/piegade/">šeit</a>
                                </div>
                            </div>
                        </div>
                        <div class="product-description">
                            <?php
                                $content = apply_filters( 'the_content', $post->post_content );
                                echo $content;
                            ?>
                        </div>

                        <?php if (count($fieldvals)) : ?>
                            <?php $counter = 0; ?>
                            <div class="overview-wrapper-mobile">
                                <h2>Produkta raksturojums</h2>
                                <div class="accordion">
                                    <?php foreach ($fieldnames as $fieldname) { 
                                        if ($fieldvals[$fieldname]) {
                                    ?>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading_<?= $fieldname; ?>">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse_<?= $fieldname; ?>" aria-expanded="false" aria-controls="collapse_<?= $fieldname; ?>">
                                                    <?php
                                                        if ($fieldname == 'how_to_use') echo 'Lietošana';
                                                        if ($fieldname == 'consists') echo 'Sastāvs';
                                                    ?>
                                                </button>
                                            </h2>
                                            <div id="collapse_<?= $fieldname; ?>" class="accordion-collapse collapse" aria-labelledby="heading_<?= $fieldname; ?>" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <?php
                                                        $field = apply_filters( 'the_content', $fieldvals[$fieldname] );
                                                        echo $field;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                        $counter++;
                                        }
                                    } ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if (count($product_prop_processed)) : ?>
                            <h3>PRODUKTA ĪPAŠĪBAS</h3>
                            <div class="prop-list row">

                                <?php foreach ($product_prop_processed as $prop) {
                                    echo sprintf('<div class="the-prop col-6"><div class="icon" style="background-image:url(%s)"></div><span>%s</span></div>',
                                        $prop['icon'],
                                        $prop['name']
                                );
                                } ?>

                            </div>
                        <?php endif; ?>
                        <div class="row">
                            <?php
                                $linked_posts = get_post_meta($post->ID, 'connected_posts', true);
                                if ($linked_posts && count($linked_posts)) :
                            ?>
                            <div class="col-6">
                                <h3>Saistītas publikācijas</h3>
                                <?php
                                    foreach($linked_posts as $linked_post) {
                                        $title = get_the_title( $linked_post );
                                        $link = get_permalink( $linked_post );
                                        echo '<p><a href="'.$link.'">'.$title.'</a></p>';
                                    }
                                ?>
                            </div>
                            <?php endif; ?>

                            <?php
                                $leaflet = get_post_meta($post->ID, 'leaflet', true);
                                $data_sheet = get_post_meta($post->ID, 'data_sheet', true);
                                $certificate = get_post_meta($post->ID, 'certificate', true);
                                if ($leaflet || $data_sheet || $certificate) :
                            ?>
                            <div class="col-6">
                                <h3>Lejuplādēt dokumentus</h3>
                                <?php 
                                    if ($leaflet) echo '<p><a href="'.wp_get_attachment_url($leaflet).'">Informatīvā lapa</a></p>';
                                    if ($data_sheet) echo '<p><a href="'.wp_get_attachment_url($data_sheet).'">Informatīvā lapa</a></p>';
                                    if ($certificate) echo '<p><a href="'.wp_get_attachment_url($certificate).'">Sertifikāts</a></p>';
                                ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
                <?php
                    $upsell_ids = $product->get_upsell_ids();
                    if ($upsell_ids && count($upsell_ids)) : 
                        echo '<h2>Saistītie produkti</h2>';
                        $args = array(
                            'post_type' => 'product',
                            'post_status' => 'publish',
                            'post__in' => $upsell_ids
                        );

                        $up_posts = new WP_Query($args);
                        
                        if ($up_posts->have_posts()) :
                            echo '<div class="row cat-items">';
                            while ($up_posts->have_posts() ) :
                                $up_posts->the_post();
                                wc_get_template_part( 'content', 'product' );
                            endwhile;
                            echo '</div>';
                        endif;
                        wp_reset_query();
                    endif;
                ?>
                <?php
                    $viewed_str = $_COOKIE["viewed"];
                    if ($viewed_str) {
                        $viewed_arr = explode('|', $viewed_str);
                        echo '<h2>nesen skatītie produkti</h2>';
                        $args = array(
                            'post_type' => 'product',
                            'post_status' => 'publish',
                            'post__in' => $viewed_arr,
                            'posts_per_page' => 4
                        );
                        $up_posts = new WP_Query($args);

                        if ($up_posts->have_posts()) :
                            echo '<div class="row cat-items">';
                            while ($up_posts->have_posts() ) :
                                $up_posts->the_post();
                                wc_get_template_part( 'content', 'product' );
                            endwhile;
                            echo '</div>';
                        endif;
                        wp_reset_query();
                    }
                    
                ?>
            </div>
		<?php endwhile; // end of the loop. ?>
        <div class="container">
            <?php get_template_part( 'template/contact', 'form' ); ?>
            </div>

	
<?php
get_footer(  );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

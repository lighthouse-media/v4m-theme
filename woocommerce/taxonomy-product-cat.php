<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */


?>
    <div class="container">
    <?php echo get_template_part( 'template/cat', 'header' ); ?>
    
	<?php echo get_template_part( 'template/cat', 'sub_categories' ); ?>

    <?php
        $cat_slug = get_query_var('product_cat');
        $cat = get_term_by('slug', $cat_slug, 'product_cat');
        
        //var_dump( $cat);

        $args = array(
            'hide_empty' => false,
            'parent' => $cat->term_id,
            'exclude'=>15
        );
        $child_terms = get_terms('product_cat', $args);
        if ( !count($child_terms) ) :
    ?>
            <?php echo get_template_part( 'template/cat', 'items' ); ?>

    <?php endif; ?>

    <?php
        $additional_items_arr = get_term_meta($cat->term_id, 'additional_category_products', true);
        if ($additional_items_arr && count($additional_items_arr) && !count($child_terms) ) {
            echo '<h2>PAPILDUS PRODUKTI</h2>';
            $args = array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'post__in' => $additional_items_arr
            );
            $up_posts = new WP_Query($args);

            if ($up_posts->have_posts()) :
                echo '<div class="row cat-items">';
                while ($up_posts->have_posts() ) :
                    $up_posts->the_post();
                    wc_get_template_part( 'content', 'product' );
                endwhile;
                echo '</div>';
            endif;
            wp_reset_query();
        }
        
    ?>
    <?php get_template_part( 'template/contact', 'form' ); ?>
    </div>
<?php
get_footer();

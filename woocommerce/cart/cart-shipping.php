<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

$formatted_destination    = isset( $formatted_destination ) ? $formatted_destination : WC()->countries->get_formatted_address( $package['destination'], ', ' );
$has_calculated_shipping  = ! empty( $has_calculated_shipping );
$show_shipping_calculator = ! empty( $show_shipping_calculator );
$calculator_text          = '';
$weight = WC()->cart->get_cart_contents_weight();
$selected_shipping = WC()->session->get('chosen_shipping_methods' );

?>
<tr class="woocommerce-shipping-totals shipping">
	
	<td colspan="2" data-title="<?php echo esc_attr( $package_name ); ?>">
		<?php if ( $available_methods ) : ?>

            <?php
                $omniva_available = isset($available_methods['omnivalt_pt']);
                
                /**helper start **/
                $totals = WC()->cart->get_totals();
                $msg = '(līdz 25kg)';
            
                
                /**helper end */
            ?>
            <?php /* if (is_cart()) : */ ?>
            <h4>Izvēlies piegādes veidu</h4>
            <div class="shipping-option-selector">
                <div class="shipping-option<?php echo $omniva_available ? '' : ' disabled'; ?><?php echo $selected_shipping[0] == 'omnivalt_pt' ? ' selected' : ''; ?>">
                    <?php if (isset($available_methods['omnivalt_pt'])) {
                        $method = $available_methods['omnivalt_pt'];
                    } ?>
                    <div class="shipping-option-title" data-method='omnivalt_pt'>
                        <span class="title">Omniva piegāde</span>
                        <span class="disabled-description"><?php echo $msg; ?></span>
                        <?php if (isset($available_methods['omnivalt_pt'])) : ?>
                            <span class="price">€<?php echo $method ? number_format($method->get_cost(), 2, ',', '.') : '' ; ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="shipping-option-form omniva">
                        <?php
                            if (isset($available_methods['omnivalt_pt'])) {
                                $method = $available_methods['omnivalt_pt'];
                                printf( '<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />', $index, esc_attr( sanitize_title( $method->id ) ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ) );
                                //printf( '<label for="shipping_method_%1$s_%2$s">%3$s</label>', $index, esc_attr( sanitize_title( $method->id ) ), wc_cart_totals_shipping_method_label( $method ) ); // WPCS: XSS ok.
						        do_action( 'woocommerce_after_shipping_rate', $method, $index );
                            } 
                        ?>
                    </div>
                </div>

                <?php 
                    $totals = WC()->cart->get_totals();
                    $totals_before_coupon = $totals["total"];
                    //$method = $weight >= 100 || $totals_before_coupon >= 20 ? $available_methods['free_shipping:1'] : $available_methods['flat_rate:2'];


                    if ( $weight >= 100 ) {
                        $method = $available_methods['free_shipping:1'];
                        $method_name = 'free_shipping:1';
                    } /* else if ( $totals_before_coupon >= 20 ) {
                        $method = $available_methods['free_shipping:4'];
                        $method_name = 'free_shipping:4';
                    } */
                    else {
                        $free_delivery = check_if_free_delivery_coupon_applied();
                        if ( $free_delivery ) {
                            $method = $available_methods['free_shipping:4'];
                            $method_name = 'free_shipping:4';
                        } else {
                            $method = $available_methods['flat_rate:2'];
                            $method_name = 'flat_rate:2';
                        }
                    }

                    set_shipping_method();
                ?>
                <div class="shipping-option<?php echo $selected_shipping[0] == 'free_shipping:1' || $selected_shipping[0] == 'flat_rate:2'  || $selected_shipping[0] == 'free_shipping:4' ? ' selected' : ''; ?>" data-method="<?php echo $method_name; ?>">
                    
                    <div class="shipping-option-title">
                        <span class="title">
                        <?php
                            if ($weight >= 100) { echo 'Kravas piegāde (Vairāk kā 100 kg)'; }
                            //else if ( $totals_before_coupon >= 20 ) { echo 'Bezmaksas kurjera piegāde'; }
                            else { echo 'Kurjera piegāde'; }
                        ?>
                        </span>
                        <span class="price">
                            <?php echo $selected_shipping[0] == 'free_shipping:1' ? '<span class="free-icon-wrapper"><span class="free-icon"></span><span class="free-icon-txt">Pircējam var būt nepieciešams nodrošināt kravas izkraušanu!</span></span>' : ''; ?>
                            <?php /* echo $totals_before_coupon >= 20 ? '<span class="free-icon-wrapper"><span class="free-icon"></span><span class="free-icon-txt">Pirkumiem virs 20EUR piegāde ir bezmaksas</span></span>' : ''; */ ?>
                            €<?php echo number_format($method->get_cost(), 2, ',', '.'); ?>
                        </span>
                    </div>
                    
                    <div class="shipping-option-form courier">
                        <?php
                            
                            printf( '<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />', $index, esc_attr( sanitize_title( $method->id ) ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ) );
                            //printf( '<label for="shipping_method_%1$s_%2$s">%3$s</label>', $index, esc_attr( sanitize_title( $method->id ) ), wc_cart_totals_shipping_method_label( $method ) ); // WPCS: XSS ok.
                            do_action( 'woocommerce_after_shipping_rate', $method, $index );
                        ?>
                        <p class="woocommerce-shipping-destination">
                        </p>
                        
                        <?php /* woocommerce_shipping_calculator( $calculator_text ); */ ?>
                        
                    </div>

                </div>

                <?php
                    $method_name = 'free_shipping:7';
                    $method = $available_methods[$method_name];
                ?>
                <div class="shipping-option<?php echo $selected_shipping[0] == 'free_shipping:7'  ? ' selected' : ''; ?>" data-method="<?php echo $method_name; ?>">
                    
                    <div class="shipping-option-title">
                        <span class="title">
                        <?php
                            echo $available_methods['free_shipping:7']->label;
                        ?>
                        </span>
                        <span class="price">
                            <?php echo $selected_shipping[0] == 'free_shipping:7' ? '<span class="free-icon-wrapper"><span class="free-icon"></span><span class="free-icon-txt">Ierodoties Vilomix teritorijā lūgums zvanīt +371 29803510 (darba dienās 8.00-16.00) preču saņemšanai</span></span>' : ''; ?>
                            €<?php echo number_format($method->get_cost(), 2, ',', '.'); ?>
                        </span>
                    </div>
                    
                    <div class="shipping-option-form courier">
                            <?php    
                                printf( '<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />', $index, esc_attr( sanitize_title( $method->id ) ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ) );
						        do_action( 'woocommerce_after_shipping_rate', $method, $index );
                            ?>
                        <?php /* woocommerce_shipping_calculator( $calculator_text ); */ ?>
                        
                    </div>

                </div>
                
            </div>

            <? /* endif; */ ?>

            <?php /* if (!is_cart()) {

                $method = $available_methods[$selected_shipping[0]];

                echo '<b>piegāde</b><br>';
                echo $method->get_label();


                echo '</td><td>';

                echo '€' . $method->get_cost();

            }  */?>
			
			<?php
		elseif ( ! $has_calculated_shipping || ! $formatted_destination ) :
			if ( is_cart() && 'no' === get_option( 'woocommerce_enable_shipping_calc' ) ) {
				echo wp_kses_post( apply_filters( 'woocommerce_shipping_not_enabled_on_cart_html', __( 'Shipping costs are calculated during checkout.', 'woocommerce' ) ) );
			} else {
				echo wp_kses_post( apply_filters( 'woocommerce_shipping_may_be_available_html', __( 'Enter your address to view shipping options.', 'woocommerce' ) ) );
			}
		elseif ( ! is_cart() ) :
			echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'There are no shipping options available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
		else :
			// Translators: $s shipping destination.
			echo wp_kses_post( apply_filters( 'woocommerce_cart_no_shipping_available_html', sprintf( esc_html__( 'No shipping options were found for %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' ) ) );
			$calculator_text = esc_html__( 'Enter a different address', 'woocommerce' );
		endif;
		?>

		<?php if ( $show_package_details ) : ?>
			<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
		<?php endif; ?>

		
	</td>
</tr>

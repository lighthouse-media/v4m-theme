<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php do_action( 'wpo_wcpdf_before_document', $this->get_type(), $this->order ); ?>


<?php
    // var_dump($this);
    $company_type = $this->order->get_meta('_billing_wooccm11');
    $shipping_method = $order->get_shipping_method();

    //var_dump($shipping_method);

    if ( $company_type == 'Juridiska persona' ) {
        $regulations_url = get_home_url() . '/precu-pardosanas-noteikumi/';
    } else {
        $regulations_url = get_home_url() . '/precu-pardosanas-noteikumi-pateretajiem/';
    }

    $order_id = $this->order->get_id();

    $order_data = $this->order->get_data();
/*     echo '<pre>';
    var_dump($order_data);
    echo '</pre>'; */

    if ($company_type == 'Juridiska persona') {
        $client = $this->order->get_billing_company();
        $pvn = $this->order->get_meta('_billing_wooccm12');
    } else {
        $client = $this->order->get_billing_first_name() . ' ' . $this->order->get_billing_last_name();
    }

    $billing_address_full = $this->order->get_billing_city() . ', ' . $this->order->get_billing_address_1() . $this->order->get_billing_address_2() . ', ' . $this->order->get_billing_postcode();

    if ( $company_type == 'Juridiska persona' ) {
        if ( $shipping_method !== 'Omniva pakomāts' && $shipping_method !== 'Omniva parcel terminal' &&  $shipping_method !== "Saņemt Vilomix birojā" ) {
            if ( $this->order->get_meta('_billing_wooccm17') && $this->order->get_meta('_billing_wooccm17') == 'Yes' ) {
                $shipping_address_full = $this->order->get_meta('_billing_wooccm18') . ', ' . $this->order->get_meta('_billing_wooccm19') . ', ' . $this->order->get_meta('_billing_wooccm20');
            } else {
                $shipping_address_full = $billing_address_full;
            }
        } 
    } else {
        if ( $shipping_method !== 'Omniva pakomāts' && $shipping_method !== 'Omniva parcel terminal' ) {
            $shipping_address_full = $billing_address_full;
        } else if ( $shipping_method == "Saņemt Vilomix birojā" ) {
            $shipping_address_full = '';
        }
    }

    

    $client_type = $this->order->get_meta('_billing_wooccm11');
    

?>

<table class="container" style="width: 100%">
    <tr>
        <td><img class="vilomix-logo" src="<?= get_stylesheet_directory_uri(); ?>/src/vilomix.png"></td>
        <td>Pavadzīme</td>
        <td>VM</td>
        <td>Nr. <span class="borderd" style="border: 1px solid #000;"><?= $this->order->get_id(); ?></span></td>
    </tr>
    <tr>
        <?php setlocale(LC_TIME, "lv_LV"); ?>

        <?php
        
            //$main_date = utf8_encode(strftime("%Y. gada, %d. %B", strtotime($this->order->get_date_modified())));
            $main_date = utf8_encode(strftime("%Y. gada, %d. ", strtotime($this->order->get_date_modified())));

            $inv_date = $this->order->get_date_modified();
            $month = date("m",strtotime($inv_date));
            
            switch ($month) {
                case '01':
                    $month_name = 'janvāris'; break;
                case '02':
                    $month_name = 'februāris'; break;
                case '03':
                    $month_name = 'marts'; break;
                case '04':
                    $month_name = 'aprīlis'; break;
                case '05':
                    $month_name = 'maijs'; break;
                case '06':
                    $month_name = 'jūnijs'; break;
                case '07':
                    $month_name = 'jūlijs'; break;
                case '08':
                    $month_name = 'augusts'; break;
                case '09':
                    $month_name = 'septembris'; break;
                case '10':
                    $month_name = 'oktobris'; break;
                case '11':
                    $month_name = 'novembris'; break;
                case '12':
                    $month_name = 'decembris'; break;
            }


        
        ?>
        
        <td colspan="4" style="text-align:center"><?= $main_date . $month_name; ?></td>
        
    </tr>
</table>

<table class="container" style="border-bottom:1px solid #000;width: 100%">
    <tr>
        <td>Preču nosūtītājs</td>
        <td>SIA Vilomix Baltic</td>
    </tr>
    <tr>
        <td>Juridiskā adrese un preču izsniegšanas adrese</td>
        <td>Bērziņi, Tumes pagasts, Tukuma novads</td>
    </tr>
    <tr>
        <td>Norēķinu rekvizīti</td>
        <td>AS Swedbank<br>HABALV22<br>LV50HABA0551018124634</td>
    </tr>
    <tr>
        <td>Licence</td>
        <td>Dzīvnieku barības reģ.Nr.ALV018936</td>
    </tr>
    <tr>
        <td>Sertifikāts</td>
        <td>BIO NR.04-188-13/19-2 LV-BIO-01</td>
    </tr>
</table>

<?php 
    /* $omnivalt_labels = new OmnivaLt_Labels();
    $test = $omnivalt_labels->print_labels( 904, false ); */

    


    //var_dump($order->get_shipping_method());
    
    /* $order_data = $this->order->get_data();
    echo '<pre>';
    var_dump($order_data);
    echo '</pre>'; */

    /*$meta_data = $this->order->get_meta('_billing_wooccm11');
    echo '<pre>';
    var_dump($meta_data);
    echo '</pre>'; */
    
    ///do_action( 'woocommerce_admin_order_data_after_shipping_address', $order );
    /* $omniva_terminal;
    ob_start();

    do_action( 'woocommerce_admin_order_data_after_shipping_address', $order );
    $omniva_terminal = ob_get_contents();

    ob_end_clean();

    ob_start(); */

?>
  
<table class="container" style="border-bottom:1px solid #000;width: 100%">

    <?php 

    echo sprintf('
        <tr><td>Preču saņēmējs:</td><td>%s</td></tr>',
        $client
    );
    
    if ($client_type == 'Juridiska persona') {

        echo sprintf('
            <tr><td>PVN:</td><td>%s</td></tr>
            <tr><td>Juridiskā adrese:</td><td>%s</td></tr>',
            $pvn,
            $billing_address_full
        );  
    }

    echo sprintf('
        <tr><td>Piegādes veids:</td><td>%s</td></tr>',
        $shipping_method
    );
    
    //var_dump(OmnivaLt_Order::admin_order_display($this->order, false));

    if ( $shipping_method == 'Omniva pakomāts' || $shipping_method == 'Omniva parcel terminal' ) {
        echo '<tr><td>Piegādes adrese:</td><td>';
        //OmnivaLt_Order::admin_order_display($this->order, false);
        echo OmnivaLt_Terminals::get_terminal_address($this->order);
        echo '</td></tr>';
    } else if ($shipping_method == 'Saņemt Vilomix birojā') {
        echo '<tr><td colspan="2" style="font-weight:700">Ierodoties Vilomix teritorijā lūgums zvanīt +371 29803510 (darba dienās 8.00-16.00) preču saņemšanai</td></tr>';
    } else {
        echo '<tr><td>Piegādes adrese:</td><td>' . $billing_address_full . '</td></tr>';
    }
    ?>


</table>

<table class="container" style="border-bottom: 4px solid #000; width: 100%">
    <tr>
        <td>
            Saimnieciskā darījuma apraksts-preču pārdošana citam uzņēmumam<br>
        </td>
        <td>Kontaktpersona</td>
    </tr>
    <tr>
        <?php if ($order_data['payment_method'] == 'bacs') { ?>
            <td>
                <b>Lūdzam apmaksāt rēķinu nākamo 5 dienu laikā, pretējā gadījumā rēķins tiks atcelts.</b>
            </td>
        <?php } else { ?>
            <td>
                Samaksāts: <?= strftime("%d.%m.%Y", strtotime($this->order->get_date_modified())); ?>.<br>
            </td>
        <?php } ?>
        <td>
            <?php
                /*if ($pvn) {
                    echo $this->order->get_meta('_billing_wooccm13');
                } else {
                    echo $this->order->get_billing_first_name() . ' ' . $this->order->get_billing_last_name();
                }*/
                echo $this->order->get_billing_first_name() . ' ' . $this->order->get_billing_last_name();
            ?><br>
            <?php if ($this->order->get_billing_phone()) : ?> Tālr: <?= $this->order->get_billing_phone(); ?><br> <?php endif; ?>
            <?php if ($this->order->get_billing_email()) : ?> Epasts: <?= $this->order->get_billing_email(); ?> <?php endif; ?>
        </td>
    </tr>
</table>

<?php
    $products = array( 132, 135, 138 );
    $total_items = 0;
    foreach ( $this->get_order_items() as $item_id => $item ) {
        //var_dump($item);
        if ( in_array( $item["product_id"], $products ) ) {
            $total_items += $item["quantity"];
        }
    }
    
    $naunau_disc = $total_items >= 2;

?>


<table class="container" style="width: 100%">
    <tr>
        <th>Kods</th>
        <th>Nosaukums</th>
        <th>Gab.</th>
        <th>Daudz.</th>
        <th>Mērv.</th>
        <th>Cena</th>
        <th>Bez atl.</th>
        <th>Atlaide</th>
        <th>Summa</th>
    </tr>
    <pre>
    <?php
        /* $total_brutto_weight = 0;
        $total_netto_weight = 0; */
        $total_weight = 0;
        $total_price = 0;
        $total_discount = 0;
        $totals = $this->order->get_order_item_totals();
    ?>
    </pre>
    <?php foreach ( $this->get_order_items() as $item_id => $item ) : ?>
            <?php 
                $product = wc_get_product( $item["product_id"] );
                
                $is_nau_nau = in_array( $item["product_id"], $products );
                
                $weight = $product->get_weight();
                /* $brutto_weight = $product->get_weight() ? $product->get_weight() : 1;
                $netto_weight = get_post_meta($product->get_id(), 'netto_weight', true) ? get_post_meta($product->get_id(), 'netto_weight', true) : $brutto_weight; */

                $total_weight += $weight * $item['quantity'];
                

                if ($product->get_sale_price() ) {
                    $regular_price = $product->get_regular_price();
                    $sale_price = $product->get_sale_price();
                } else if ( $is_nau_nau && $naunau_disc ) {
                    $regular_price = $product->get_regular_price();
                    $sale_price = $regular_price * 0.7;
                } else {
                    $regular_price = $sale_price = $product->get_regular_price();
                }

                $diff = $regular_price - $sale_price;
                
                //var_dump($total_price);


                $diff_per_position = $diff * $item['quantity'];

                $position_total = floatval($item['item']->get_total());
                
                
                $position_subtotal = floatval($item['item']->get_subtotal());
                $position_additional_discount = 0;

                if ( $position_total !== $position_subtotal ) {
                    $position_additional_discount = $position_subtotal - $position_total;
                }

                $total_position_discount = $diff_per_position + $position_additional_discount;
                $total_discount += $total_position_discount;
                
                if ( $is_nau_nau && $naunau_disc ) {
                    $position_total = $position_total * 0.7;
                }

                $total_price += $position_total;

            ?>


<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', 'item-'.$item_id, $this->get_type(), $this->order, $item_id ); ?>">
                <td><?= $item['sku']; ?></td>
                <td><?= $item['name']; ?></td>
                <td><?= $item['quantity']; ?></td>
                <td style="text-align: right;"><?= $weight * $item['quantity']; ?></td>
                <td>kg</td>
                <td><?= number_format($regular_price / 1.21, 2, ',', '.'); ?></td>
                <td><?= number_format($regular_price * $item['quantity'] / 1.21, 2, ',', '.'); ?></td>
                <td><?= number_format($total_position_discount / 1.21, 2, ',', '.'); ?></td>
                <td><?= number_format($position_total / 1.21, 2, ',', '.'); ?></td>
				
			</tr>
    <?php endforeach; ?>

    <?php
        // Totals and discounts
        $nopvn = $total_price / 1.21;
        
        $amount_discount = 0;
        $fees = $this->order->get_fees();
        //var_dump($fees);
        
        if (count($fees)) {
            foreach( $fees as $fee ) {
                $the_fee = $fee->get_data();
                //var_dump($the_fee);
                //var_dump($the_fee["name"] != 'NauDiscount');
                if ( $the_fee["name"] != 'Ņau Ņau Akcija' ) {
                    $amount_discount += $fee->get_amount() * -1;
                }
            }
        }

       

        $order_discount = $order_data['discount_total'];
        $shipping = $order_data['shipping_total'];
        
        
        /* echo '<pre>';
        var_dump($order_data);
        echo '</pre>'; */


        $grandtotal_no_pvn = $nopvn - $amount_discount / 1.21 + $shipping / 1.21;

        //var_dump($grandtotal_no_pvn);

        $pvn = $order_data['total'] - $grandtotal_no_pvn;
    ?>
    <tr>
        <td colspan="3" style="text-align: right;">Neto svars:</td>
        <td style="text-align: right;"><?= number_format($total_weight, 2); ?></td>
        <td>kg</td>
        <td colspan="2" style="text-align: right;">Atlaide pasūtījumam:</td>
        <td ><?php echo number_format($total_discount / 1.21, 2, ',', '.'); ?></td>

    </tr>

    <tr>
        <td colspan="8" style="text-align: right;">Kopā bez PVN:</td>
        <td><?= number_format($nopvn, 2); ?> €</td>
    </tr>

    <?php if ($order_data['discount_total'] !== '0') : ?>
        <!-- <tr>
            <td colspan="8" style="text-align: right;">Atlaide pasūtījumam bez PVN:</td>
            <td><?= number_format($order_data['discount_total'] / 1.21, 2); ?> €</td>
        </tr> -->
    <?php endif; ?>

    <?php
        $fees = $this->order->get_fees();
        if ($amount_discount) 
        foreach( $fees as $fee ) { ?>
            <tr>
                <td colspan="8" style="text-align: right;">Apjoma atlaide  bez PVN:</td>
                <td><?= number_format(($amount_discount*-1) / 1.21, 2); ?> €</td>
            </tr>
    
    <?php } ?>

    <tr>
        <td colspan="8" style="text-align: right;">Piegādes cena bez PVN:</td>
        <td><?= number_format($order_data['shipping_total'] / 1.21, 2); ?> €</td>
    </tr>
    
    
    <tr>
        <td colspan="8" style="text-align: right;">PVN (21%):</td>
        <td><?= number_format($pvn, 2); ?> €</td>
    </tr>
    
    <tr>
        <td colspan="8" style="text-align: right;">Summa apmaksai:</td>
        <td><?= number_format($order_data['total'], 2); ?> €</td>
    </tr>
</table>
<table style="margin-top: 3rem;">
    
    <tr>
        <td>Pircējs piekrīt, ka preču piegāde tiek veikta saskaņā ar V4M.LV <a href="<?= $regulations_url; ?>">piegādes un veikala noteikumiem</a>. Pavadzīme ir sastādīta elektroniski pēc pirkuma apmaksas un tā ir derīga bez Pušu paraksta.</td>
    </tr>
</table>

<?php  /* var_dump($this->get_billing_address_1()) */
?>
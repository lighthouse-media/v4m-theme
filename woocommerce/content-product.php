<?php
$product = wc_get_product( $post->ID );
$img = $product->get_image();
$title = $product->get_name();
$desc = $product->get_short_description();
$type = $product->get_type();
$leftover = $product->get_stock_quantity();
$price = $product->get_price();
$original_price = $product->get_sale_price() ? $product->get_regular_price() : 0;
$is_popular_product = get_post_meta($post->ID, 'is_popular_product', true);
$non_gmo = get_post_meta($post->ID, 'non_gmo', true);

$link = get_permalink( $post->ID );
$id = $post->ID;

$atts = $product->get_attributes();
$product_properties = $atts && array_key_exists("pa_property", $atts) ? $atts["pa_property"]['options'] : false;
$product_prop_processed = [];
if ($product_properties && count($product_properties)) {
    foreach($product_properties as $prop_id) {
        $the_prop = [];
        $the_term = get_term( $prop_id, 'pa_property' )->name;
        $the_icon_id = get_term_meta( $prop_id, 'icon', true );
        $the_icon = wp_get_attachment_url($the_icon_id);
        $the_prop['name'] = $the_term;
        $the_prop['icon'] = $the_icon;
        array_push($product_prop_processed, $the_prop);
    }
}

$cart = WC()->cart;
$cart_amounts = $cart->get_cart_item_quantities();
$amount = array_key_exists($id, $cart_amounts) ?  $cart_amounts[$id] : 0;

if ($type == 'variable') {
    $fist_var_id = $product->get_children()[0];
    $first_var = wc_get_product($fist_var_id);
    $price = $first_var->get_price();
    $leftover = $first_var->get_stock_quantity();
    $id = $fist_var_id;
}
?>

<div class="product-block-wrapper col-xs-12 col-sm-6 col-md-4 col-lg-3 product-<?= $type; ?>">
    <div class="product-wrapper">
        <div class="helper-container">
            <a href="<?= $link; ?>">
                <div class="img-wrapper">
                    <?= $img; ?>
                    <?php if ($original_price){
                        $add_class = $is_popular_product ? ' is-pop' : '';
                        echo '<div class="has-discount' . $add_class . '">-' . number_format(((1-$price/$original_price)*100), 0) . '%</div>';
                    } ?>
                    <?php if ($is_popular_product) {

                        echo '<div class="popular-product"></div>';
                    } ?>
                    <?php
                        if ($non_gmo) {
                            echo sprintf('<div class="non-gmo%s%s"></div>',
                                $is_popular_product && $original_price ? ' two-front' : '',
                                $is_popular_product || $original_price ? ' one-front' : ''
                            );
                        }
                    ?>
                </div>
            </a>
            <a href="<?= $link; ?>"> 
                <h3><?= $title; ?></h3>
                <p class="short-desc"><?= $desc; ?></p>
            </a>
        </div>
        <div class="helper-container">
            <div class="price-and-leftover">
                <div class="price">
                    <?php if ($original_price) echo "<div class=\"original-price\">€$original_price</div>"; ?>
                    <span class="actual-price<?php if ($original_price) echo ' has-discount'; ?>">€<?= number_format($price, 2); ?></span>
                </div>
                <div class="leftover <?= $leftover ? 'in-stock' : 'not-in-stock'; ?>">
                    <?php
                        echo show_stock($leftover);
                    ?>
                </div>
            </div>
            <div class="prop-list">
                <?php if (count($product_prop_processed)) : ?>
                    <!-- <div class="prop-list"> -->

                        <?php foreach ($product_prop_processed as $prop) {
                            echo sprintf('<div class="the-prop"><div class="icon" style="background-image:url(%s)"></div><span>%s</span></div>',
                                $prop['icon'],
                                $prop['name']
                        );
                        } ?>

                    <!-- </div> -->
                <?php endif; ?>
            </div>
            
            <div class="order-setup<?= $amount ? ' in-basket' : '' ?>">
                <?php if (false) : ?>
                    <p class="pack-size"><b>Apjoms:</b> <?= get_display_unit($product); ?></p>
                <?php endif; ?>
                <?php if ($type == 'variable') : ?>
                    <div class="pack-size-blk">
                        <p class="pack-size"><b>Apjoms:</b></p>
                        <select name="unit" class="unit">
                            <?php
                                $variation_ids = $product->get_children();
                                foreach( $variation_ids as $var_id ) {
                                    $var = wc_get_product($var_id);
                                    $unit = $var->get_attribute( 'unit' );
                                    $price = $var->get_price();
                                    $stock = $var->get_stock_quantity();
                                    echo '<option data-id="' . $var_id . '" data-stockinfo="' . show_stock($stock) . '" data-stock="' . $stock . '" data-price="' . $price . '" value=' . $unit . ">" . get_display_unit($var, $product) . "</option>" ;
                                
                                }
                            ?>
                        </select>
            
                    </div>
                <?php endif; ?>
                <div class="equalizer-amount">
                    <div class="amount<?php if (!$leftover) echo ' hidden' ?>">
                        <div class="label">Daudzums: </div>
                        <div class="amount-controls product-amount" data-max="<?= $leftover ?>">
                            <div class="ctrl-btn" data-ctrl="-1">-</div>
                            <div class="amount"><input type="number" value="1" min="0" max="<?= $amount ? $leftover - $amount : $leftover; ?>" ></div>
                            <!--<div class="amount-display" data-value="1">1</div>-->
                            <div class="ctrl-btn" data-ctrl="1">+</div>
                        </div>
                    </div>
                    <?php
                        echo '<div class="in-basket">';
                        if ($amount) {
                            echo sprintf('Grozā %s', $amount );
                        }
                        echo '</div>';
                    ?>
                </div>
                <div
                    class="add-to-basket"
                    data-instock="Ielikt grozā"
                    data-link="<?= $link; ?>"
                    data-notinstock="Aplukot produktu"
                    data-productid=""
                    data-id="<?= $id; ?>"
                    data-amount="1"
                    data-allow="<?= $leftover ? 1 : 0 ?>"
                ><?= !$leftover ? 'Aplukot produktu' : 'Ielikt grozā' ?></div>
            </div>
        </div>
    </div>
</div>

jQuery(document).ready(function ($) {

    /*$.ajax({
        type: "post",
        url: ajaxLocal.ajaxurl,
        data: {
            action: "get_quick_cart"
        },
        success: function (response) {
            if (response == 'error') return;
            
            $('.quick-cart-wrapper').html(response);
            
        },
        error: function (e) {
        }
    })*/

    $('.woocommerce-product-gallery__image').click( function(){ 
        if (window.innerWidth > 768) return;
        //console.log('click2');
        if ($(this).hasClass('zoomed')) {
            this.scrollTo(0, 0);
        } else {
            this.scrollTo($(this).find('.zoomImg').width()/2, $(this).find('.zoomImg').height()/2 );
        }
        $(this).toggleClass('zoomed');
    });

    $(document).on('click', function () {
        if (window.innerWidth > 768) return;
        if($(this).parents('.woocommerce-product-gallery__image').length || $(this).hasClass('.woocommerce-product-gallery__image')) return;
        if ($('.woocommerce-product-gallery__image').length) {
            $('.woocommerce-product-gallery__image').removeClass('zoomed');
            $('.woocommerce-product-gallery__image')[0].scrollTo(0, 0);
        }
    })

    $(document).on('click', '.amount-controls .ctrl-btn', function () { //amount change
        const amount_field = $(this).parent('.amount-controls').find('input');
        const inc = parseInt($(this).attr('data-ctrl'));
        const c_amount = parseInt(amount_field.val());
        const max_amount = parseInt($(amount_field).attr('max'));
        let new_amount = c_amount + inc;
        if (new_amount < 1) new_amount = 1;
        if (new_amount > max_amount) new_amount = max_amount;

        $(amount_field).val(new_amount);
        $(amount_field).attr('data-value', new_amount);

    });

    $(document).on('change', '.product-amount.amount-controls input', function() {
        change_product_amount_on_pages(this);
    });

    $(document).on('keypress', '.product-amount.amount-controls input', function (e) {
        var key = e.which;
        if(key == 13) {
            change_product_amount_on_pages(this);
        }
    }); 

    function change_product_amount_on_pages(input_field) {
        const current_val = parseInt($(input_field).val());
        const max_val = parseInt($(input_field).attr('max'));
        if (current_val > max_val) $(input_field).val(max_val);
    }
    

    //DEPRECATED
    $('.unit').change(function () { //unit selection change 
        console.log(222);
        const selected_option = $(this).find('option:selected');
        const price = selected_option.attr('data-price');
        const stock = parseInt(selected_option.attr('data-stock'));
        const stock_info = selected_option.attr('data-stockinfo');
        const id = selected_option.attr('data-id');

        const parent_block = $(this).parents('.order-setup');
        parent_block.find('.price').text(new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'EUR'
        }).format(price));

        const stock_info_block = parent_block.find('.leftover');
        const amount_and_add = parent_block.find('.amount-and-add');
        stock_info_block.text(stock_info);

        const amount_block = parent_block.find('.amount');
        const c_value = amount_block.find('.amount-display');
        const add_to_basket = parent_block.find('.add-to-basket');

        parent_block.find('.amount-display').attr('data-max', stock);
        if (parseInt(c_value.attr('data-value')) > stock) {
            c_value.attr('data-value', stock);
            c_value.text(stock);
            add_to_basket.attr('data-amount', stock);
        } else if (parseInt(c_value.attr('data-value')) < 1) {
            c_value.attr('data-value', 1);
            c_value.text(1);
            add_to_basket.attr('data-amount', 1);
        }

        if (stock > 0) {
            stock_info_block.addClass('in-stock').removeClass('not-in-stock');
            add_to_basket.text(add_to_basket.attr('data-instock'));
            amount_block.removeClass('hidden');
            amount_and_add.removeClass('hidden');
            add_to_basket.attr('data-allow', 1);
        } else {
            stock_info_block.addClass('not-in-stock').removeClass('in-stock');
            add_to_basket.text(add_to_basket.attr('data-notinstock'));
            amount_block.addClass('hidden');
            amount_and_add.addClass('hidden');
            add_to_basket.attr('data-allow', 0);
        }

        add_to_basket.attr('data-id', id);

    });

    let interval;

    if ($('.slideshow-wrapper .slide').length > 1) {
        interval = setInterval(()=> {
            const active = $('.slide.active');
            const active_nav = $('.slide-link.active');
            //console.log(active.next().length);
            if (active.next().length) {
                active.next().addClass('active');
                active.next().css({'display': 'flex' });
                setTimeout(()=> {
                    active.next().css({'opacity': '1' });
                },50);
                active_nav.next().addClass('active');
                


            } else {
                $('.slide').eq(0).addClass('active');
                $('.slide').eq(0).css({'display': 'flex' });
                setTimeout(()=> {
                    $('.slide').eq(0).css({'opacity': '1' });
                },50);

                $('.slide-link').eq(0).addClass('active');
            }
            active.removeClass('active');
            active.css({'opacity': '0' });
            setTimeout(()=> {
                active.css({'display': 'none' });
            },300);

            active_nav.removeClass('active');
        },7000);
    }

    $('.slide-link').click(function(){
        if ($(this).hasClass('active')) return;
        const index = $(this).attr('data-index');
        const active_sl = $('.slide.active');
        active_sl.removeClass('active');
        $('.slide-link.active').removeClass('active');
        active_sl.css({'opacity': '0' });
        setTimeout(()=> {
            active_sl.css({'display': 'none' });
        },300);


        $('.slide').eq(index).addClass('active');
        $('.slide').eq(index).css({'display': 'flex' });
        setTimeout(()=> {
            $('.slide').eq(index).css({'opacity': '1' });
        },50);


        $('.slide-link').eq(index).addClass('active');
        clearInterval(interval);
    });

    $(document).on('click', '.add-to-basket', function () {
        console.log(123);
        const allow = parseInt($(this).attr('data-allow'));
        if (!allow) {
            document.location = $(this).attr('data-link');
            return;
        }

        const id = $(this).attr('data-id');
        const amount = $(this).parents('.order-setup').find('input').val();

        const parent_blk = $(this).parents('.order-setup');

        //console.log('add');

        $.ajax({
            type: "post",
            url: ajaxLocal.ajaxurl,
            data: {
                action: "add_to_cart",
                id,
                amount
            },
            success: function (response) {
                if (response == 'error') return;
                
                const resp = JSON.parse(response);
                //console.log(resp);
                $('.quick-cart-wrapper').html(resp.cart);
                parent_blk.addClass('in-basket');
                parent_blk.find('.in-basket').text('Grozā ' + resp.amount);
                if (resp.max_leftover) {
                    parent_blk.find('input').val(1);
                    parent_blk.find('input').attr('max', resp.max_leftover)
                } else {
                    parent_blk.find('input').val(0);
                    parent_blk.find('input').attr('max', 0);
                }
                
                

                $('body').append(resp.cart_info);
                setTimeout(()=> {
                    $('.added-to-cart-info').addClass('show');
                }, 100);

                $('.mobile-cart .items-in-cart').text(resp.in_cart);
                if (resp.in_cart) {
                    $('.mobile-cart .items-in-cart').addClass('show');
                } else { 
                    $('.mobile-cart .items-in-cart').removeClass('show');
                }

                if (resp.cart_weight > 50 && resp.cart_weight < 100) {
                    $('.mobile-weight-disclaimer').addClass('show');
                } else {
                    $('.mobile-weight-disclaimer').removeClass('show');
                }

                show_amount_discount_msg(resp.prev_price, resp.price);

                if (resp.prev_weight < 100 && resp.cart_weight >= 100 ) show_discount_message()
                
            },
            error: function (e) {
                //console.log(e);
                ///console.log('error');
                //console.log(e);
            }
        })
    });

    function show_discount_message() {
        const discount_msg = document.createElement('div');
        $(discount_msg).addClass('discount-message-wrapper');
        $(discount_msg).addClass('appear');
        $(discount_msg).html('<div class="discount-message">Esi saņēmis bezmaksas piegādi (pasūtījums virs 100kg)</div>');
        $('body').append(discount_msg);

        setTimeout(() => {
            $(discount_msg).removeClass('appear');
        }, 100);

        setTimeout(() => {
            $(discount_msg).addClass('disappear');
        }, 4000);

        setTimeout(() => {
            $(discount_msg).remove();
        }, 4400);
        
    }

    $(document).on('click', '.remove-item-button', function () { //button in quick cart click
        const id = $(this).attr('data-id');
        //console.log('remove');

        $.ajax({
            type: "post",
            url: ajaxLocal.ajaxurl,
            data: {
                action: "remove_from_cart",
                id,
            },
            success: function (response) {
                if (response == 'error') return;
                //console.log(response);
                const resp = JSON.parse(response);
                //console.log(resp);

                if (!resp.cart_page) location.reload();
                
                $('.quick-cart-wrapper').html(resp.cart);
                $('.cart-form').html(resp.cart_page);
                $('.cross-sells').html(resp.cross_sells);

                $('.mobile-cart .items-in-cart').text(resp.in_cart);
                if (resp.in_cart) {
                    $('.mobile-cart .items-in-cart').addClass('show');
                } else { 
                    $('.mobile-cart .items-in-cart').removeClass('show');
                }

                if (resp.cart_weight > 50 && resp.cart_weight < 100) {
                    $('.mobile-weight-disclaimer').addClass('show');
                } else {
                    $('.mobile-weight-disclaimer').removeClass('show');
                }
                
            },
            error: function (e) {
                //console.log(e);
            }
        })

    });

    $(document).on('click', '.shop_table .remove', function () { //button in quick cart click
        const id = $(this).attr('data-id');
        //console.log('remove');

        $.ajax({
            type: "post",
            url: ajaxLocal.ajaxurl,
            data: {
                action: "remove_from_cart",
                id,
            },
            success: function (response) {
                if (response == 'error') return;
                //console.log(response);
                const resp = JSON.parse(response);
                //console.log(resp);

                if (!resp.cart_page) location.reload();
                
                $('.quick-cart-wrapper').html(resp.cart);
                $('.cart-form').html(resp.cart_page);
                $('.cross-sells').html(resp.cross_sells);

                $('.mobile-cart .items-in-cart').text(resp.in_cart);
                if (resp.in_cart) {
                    $('.mobile-cart .items-in-cart').addClass('show');
                } else { 
                    $('.mobile-cart .items-in-cart').removeClass('show');
                }

                if (resp.cart_weight > 50 && resp.cart_weight < 100) {
                    $('.mobile-weight-disclaimer').addClass('show');
                } else {
                    $('.mobile-weight-disclaimer').removeClass('show');
                }
                
            },
            error: function (e) {
                //console.log('err');
                //console.log(e);
            }
        })

    });

    $(document).on('click', '.remove-coupons', function () { //button in quick cart click
        //console.log('remove-coupons');

        $.ajax({
            type: "post",
            url: ajaxLocal.ajaxurl,
            data: {
                action: "remove_coupons"
            },
            success: function (response) {
                location.reload();
            },
            error: function (e) {
                //console.log(e);
            }
        })

    });

    $('.nav-toggler-mobile').click( function(){
        $('nav.main-nav').toggleClass('active');
    });

    $(document).on('click', '.main-nav.active>.nav-dropdown>ul>li>a', function(e) {
        e.preventDefault();
        const url = e.target.href;
        const child_ul = $(e.target).parents('li').find('ul');
        if (!child_ul.length) return window.location = url;
        
        if (child_ul.eq(0).hasClass('opened')) return child_ul.eq(0).removeClass('opened');

        $('.main-nav.active ul ul').removeClass('opened');
        child_ul.eq(0).addClass('opened');
    });

    $(document).on('click', function(e) {
        // console.log($(e.target).parents('.nav-dropdown'));
        if (!$(e.target).parents('.main-nav').length) $('nav.main-nav').removeClass('active');
    });

    $('.mobile-search-toggler').click( function() {
        $('.nav-search .search').toggleClass('visible');
    });

    $(document).on('click', '.woocommerce-cart-form .ctrl-btn', function(){
        /*
            Get max value
            get current value
            get increament/decreament action
            try to increase the value and check if the value is more than max value
            if the value is ok - send ajax request to change value; send itemsid, new amount 
            add overlay to the cart page

            on response 
                update the quick cart informatio
                update the main cart information
                remove overlay from the cart page
        */

        const parent_blk = $(this).parents('.amount-controls');
        const max_value = parseInt(parent_blk.attr('data-max'));
        const current_value = parseInt(parent_blk.attr('data-value'));
        const id = parent_blk.attr('data-id');
        const the_action = parseInt($(this).attr('data-value'));

        const new_value = current_value + the_action;
        if (new_value < 0 || new_value > max_value) return false;

        const overlay_layer = $('.woocommerce-cart-form .overlay')
        overlay_layer.addClass('show');


        $.ajax({
            type: "post",
            url: ajaxLocal.ajaxurl,
            data: {
                action: "update_cart_page",
                id,
                new_value
            },
            success: function (response) {
                if (response == 'error') return;
                //console.log(response);
                const resp = JSON.parse(response);
                //console.log(resp.cross_sells);
                $('.quick-cart-wrapper').html(resp.cart);
                $('.cart-form').html(resp.cart_page);
                $('.cross-sells').html(resp.cross_sells);
                overlay_layer.removeClass('show');

                $('.mobile-cart .items-in-cart').text(resp.in_cart);
                if (resp.in_cart) {
                    $('.mobile-cart .items-in-cart').addClass('show');
                } else { 
                    $('.mobile-cart .items-in-cart').removeClass('show');
                }

                if (resp.cart_weight > 50 && resp.cart_weight < 100) {
                    $('.mobile-weight-disclaimer').addClass('show');
                } else {
                    $('.mobile-weight-disclaimer').removeClass('show');
                }

                show_amount_discount_msg(resp.prev_price, resp.price);

                if (resp.prev_weight < 100 && resp.cart_weight >= 100 ) show_discount_message();
            },
            error: function (e) {
                overlay_layer.removeClass('show');
               // console.log(e);
                ///console.log('error');
                //console.log(e);
            }
        })
    });

    $(document).on('change', '.woocommerce-cart-form .amount-controls input', function() {
        change_product_amount(this);
    });

    $(document).on('keypress', '.woocommerce-cart-form .amount-controls input', function (e) {
        var key = e.which;
        if(key == 13) {
            change_product_amount(this);
        }
    });

    function show_hide_bank_transfer() {
        if (!$('.ecommerce-type').length) return;
        if ($('.ecommerce-type.active').data('ecom') == 'privat') {
            $('.payment_methods.transfer').addClass('hidden');
            if ($('.payment_methods.transfer').hasClass('selected')) $('.payment_methods.cards .title').click();
        } else {
            $('.payment_methods.transfer').removeClass('hidden');
        }
    }
    //show_hide_bank_transfer();

    function show_amount_discount_msg(prev_price, price) {
        return; //block is disabled

        let amount = 0
        let basket_amount = 0;
        /*if ( prev_price < 2000 && price >= 2000 ) {
            amount = 20; basket_amount = 2000;
        } else if ( prev_price < 1500 && price >= 1500 ) {
            amount = 15; basket_amount = 1500;
        } else*/ if ( prev_price < 1000 && price >= 1000 ) {
            amount = 10; basket_amount = 1000;
        } else if ( prev_price < 500 && price >= 500 ) {
            amount = 5; basket_amount = 500;
        }

        if (!amount) return;

        const discount_msg = document.createElement('div');
        $(discount_msg).addClass('discount-message-wrapper');
        $(discount_msg).addClass('appear');
        $(discount_msg).html(`<div class=\"discount-message\">Esi saņēmis ${amount}% pirkuma apjoma atlaidi pie ${basket_amount}EUR</div>`);
        $('body').append(discount_msg);

        setTimeout(() => {
            $(discount_msg).removeClass('appear');
        }, 100);

        setTimeout(() => {
            $(discount_msg).addClass('disappear');
        }, 4000);

        setTimeout(() => {
            $(discount_msg).remove();
        }, 4400);
    }

    function change_product_amount(field) {
        const parent_blk = $(field).parents('.amount-controls');
        const max_value = parseInt(parent_blk.attr('data-max'));
        const current_value = parseInt(parent_blk.attr('data-value'));
        const id = parent_blk.attr('data-id');

        let new_value = field.value;
        if (new_value < 0 ) return false;
        if ( new_value > max_value ) {
            new_value = max_value;
        }

        const overlay_layer = $('.woocommerce-cart-form .overlay')
        overlay_layer.addClass('show');

        $.ajax({
            type: "post",
            url: ajaxLocal.ajaxurl,
            data: {
                action: "update_cart_page",
                id,
                new_value
            },
            success: function (response) {
                if (response == 'error') return;
                console.log(response);
                const resp = JSON.parse(response);
                //console.log(resp.cross_sells);
                $('.quick-cart-wrapper').html(resp.cart);
                $('.cart-form').html(resp.cart_page);
                $('.cross-sells').html(resp.cross_sells);
                overlay_layer.removeClass('show');

                $('.mobile-cart .items-in-cart').text(resp.in_cart);
                if (resp.in_cart) {
                    $('.mobile-cart .items-in-cart').addClass('show');
                } else { 
                    $('.mobile-cart .items-in-cart').removeClass('show');
                }

                if (resp.cart_weight > 50 && resp.cart_weight < 100) {
                    $('.mobile-weight-disclaimer').addClass('show');
                } else {
                    $('.mobile-weight-disclaimer').removeClass('show');
                }
            },
            error: function (e) {
                overlay_layer.removeClass('show');
                //console.log(e);
                ///console.log('error');
                //console.log(e);
            }
        })
    }

    $(document).on('click', '.shipping-option-title', function(){
        //console.log('click');
        if ($(this).parents('.shipping-option').hasClass('selected')) return;
        if ($(this).parents('.shipping-option').hasClass('disabled')) return;
        //console.log($(this).parents('.shipping-option').hasClass('disabled'));
        //console.log($(this).parents('.shipping-option').find('input.shipping_method'));
        $(this).parents('.shipping-option').find('.shipping_method').click();

        $('.shipping-option.selected').removeClass('selected');
        $(this).parents('.shipping-option').addClass('selected');

        setTimeout(()=>location.reload(), 500);
    });

    $(document).on('click', '.close-btn', function() {
        $('.added-to-cart-info').removeClass('show');
        setTimeout(()=> {
            $('.added-to-cart-info').remove();
        }, 500);
    });

    $(document).on('click', '.order-blocker', function(){
        $('.select-payment').removeClass('hidden')
    });

    $(document).on('click', '.wc_payment_methods .title', function(){
        if ($(this).parents('.wc_payment_methods').hasClass('selected')) return;

        $('.select-payment').addClass('hidden');
        $('.order-blocker').addClass('hidden');

        $('.wc_payment_methods.selected').removeClass('selected');
        $(this).parents('.wc_payment_methods').addClass('selected');
        $(this).parents('.wc_payment_methods').find('input').eq(0).click();
    })

    $(".select-type").prependTo("#customer_details .woocommerce-billing-fields .woocommerce-billing-fields__field-wrapper");

    const business_type_val = $('#billing_wooccm11').val();
    const business_type = business_type_val == 'Fiziska persona' ? 'privat' : 'jur';
    $('.ecommerce-type[data-ecom="'+ business_type +'"]').addClass('active');
    $('.select-type').addClass('show');

    $('.ecommerce-type').click(function(){
        if ($(this).hasClass('active')) return;
        const business_type = $(this).data().ecom;
        const business_type_val = business_type == 'privat' ? 'Fiziska persona' : 'Juridiska persona';
        $('#billing_wooccm11').val(business_type_val).change();
        $('.ecommerce-type.active').removeClass('active');
        $(this).addClass('active');
        //show_hide_bank_transfer();
    })

    $('.wooccm-field-wooccm15 .required').mouseenter(function(){
        if ($('.wooccm-field-wooccm15 .required .description').length) return;
        const test = document.createElement('div');
        test.classList.add('description');
        $(test).html('Jūsu personas dati tiks izmantoti, lai apstrādātu jūsu pasūtījumu, atbalstītu jūsu pieredzi visā šajā vietnē un citiem mērķiem, kas aprakstīti mūsu privātuma politikā');
        $(this).append(test);
    });

    $('.wooccm-field-wooccm14 .optional').mouseenter(function(){
        if ($('.wooccm-field-wooccm14 .optional .description').length) return;
        const test = document.createElement('div');
        test.classList.add('description');
        $(test).html('Parakstoties uz jaunumiem mēs jūs e-pastā informēsim par:<ul><li>Jauniem produktiem</li><li>Atlaižu akcijām</li><li>Vilomix organizētajiem pasākumiem”</li></ul>');
        $(this).append(test);
    });

    $('.wooccm-field-wooccm15 .required, .wooccm-field-wooccm14 .optional').mouseleave(function(){
        $('.wooccm-field-wooccm15 .required .description').remove();
        $('.wooccm-field-wooccm14 .optional .description').remove();
    })

    // COOKIES

    function setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
    }

   if ($('body').hasClass('single-product') ) {
       const id = $('.main-product-info').attr('data-prodid');
       if ( id ) {
            let viewed = getCookie('viewed');
            //console.log(viewed);
            if (!viewed) {
                setCookie( 'viewed', id, 2 );
            } else {
                const items = viewed.split('|');
                if (items.indexOf(id) !== -1) return;
                items.push(id);
                const newCookie = items.join('|');
                setCookie( 'viewed', newCookie, 2 );
            }
       }
   }

  /*  if ($('.wc_payment_method ').length) {
       const cards = document.createElement('div');
       const banks = document.createElement('div');
       const payments = document.createElement('div');
       console.log($('.wc_payment_method '));

       $(cards).append($('.wc_payment_method').eq(0));

       for (let i = 0; i<4; i++) {
           $(banks).append($('.wc_payment_method ').eq(0));
       }

       $(payments).append(cards).append(banks)

       $('.wc_payment_methods').html(payments);
       console.log(payments);

   } */

   equalize_grid_items();

   function equalize_grid_items() {

        //console.log(window.innerWidth);
        let items_in_row = window.innerWidth > 991 ? 4 : window.innerWidth > 768 ? 3 : window.innerWidth > 576 ? 2 : 1;
        //console.log(items_in_row);
       if (!$('.product-block-wrapper').length) return;
       
       const items = $('.product-block-wrapper');
       items.find('.prop-list').height('auto');
       items.find('.order-setup').height('auto');
       const rows = Math.round(items.length / items_in_row);
       for( let i = 0; i < rows; i++ ) {
            let max_height = 0;
            let max_height2 = 0;
            const slice_items = items.slice( i * items_in_row, (i+1) * items_in_row );
            for ( let n = 0; n < slice_items.length; n++ ) {
                max_height = max_height > slice_items.eq(n).find('.prop-list').height() ? max_height : slice_items.eq(n).find('.prop-list').height();
                max_height2 = max_height2 > slice_items.eq(n).find('.order-setup').height() ? max_height2 : slice_items.eq(n).find('.order-setup').height();
            }
            slice_items.find('.prop-list').height(max_height);
            slice_items.find('.order-setup').height(max_height2);
       }
   }

    // SELECT FIELD VIEW

    var x, i, j, l, ll, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("orderby");
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        /* For each element, create a new DIV that will act as the selected item: */
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /* For each element, create a new DIV that will contain the option list: */
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            /* For each option in the original select element,
            create a new DIV that will act as an option item: */
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /* When an item is clicked, update the original select box,
                and the selected item: */
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });

        b.addEventListener("click", function (e) {
            //console.dir($(this).parents('form')[0][0].value);
            //$(this).parents('form').submit();
            const new_url = updateQueryStringParameter(location.href, 'orderby', $(this).parents('form')[0][0].value);
            window.location.href = new_url;
            //console.log($(this))
        })
    }

    



    window.addEventListener("resize", () => {
		equalize_grid_items();
        change_slide_images();
        header_helper();
    });

    function header_helper() {
        const header_heigth = $('.site-header').height();
        $('.header-helper').height(header_heigth);
    };
    header_helper();

    function change_slide_images() {
        const slides = $('.slide-img');
        if (!slides) return;

        const width = window.innerWidth;
        let source = width <= 576 ? 'data-vertical' : 'data-horizontal';

        slides.each( function() {
            $(this).css("background-image", "url(" + $(this).attr(source) + ')');
        })
    }
    change_slide_images();

    function closeAllSelect(elmnt) {
        /* A function that will close all select boxes in the document,
        except the current select box: */
        var x, y, i, xl, yl, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        xl = x.length;
        yl = y.length;
        for (i = 0; i < yl; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < xl; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }

    /* If the user clicks anywhere outside the select box,
    then close all select boxes: */
    document.addEventListener("click", closeAllSelect);


    document.addEventListener('scroll', ()=> {
        if (window.scrollY > 50 ) {
            $('.site-header').addClass('not-on-top');
        } else { $('.site-header').removeClass('not-on-top'); }
    })

    $('#menu-information').unwrap('.menu-information-container');

});

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
      return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
      return uri + separator + key + "=" + value;
    }
}
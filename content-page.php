<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package storefront
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	/**
	 * Functions hooked in to storefront_page add_action
	 *
	 * @hooked storefront_page_header          - 10
	 * @hooked storefront_page_content         - 20
	 */
	//do_action( 'storefront_page' );
	?>

    <header class="entry-header">   
        <?php
        
            $hide_header = get_post_meta( $post->ID, 'hide_header', true );
            if (!$hide_header)
                echo sprintf('<h1>%s</h1>', get_the_title( $post ));
        ?>
    </header>
    <div class="entry-content">
        <?php the_content(); ?>
    </div>
</article><!-- #post-## -->

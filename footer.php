<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer class="site-footer" role="contentinfo">
		<div class="container">
            <div class="row">
                <div class="col-12 col-md-4 footer-col">

                    <p><strong>Vilomix Baltic SIA</strong><br>
                    <b>Adrese​:</b> "Bērziņi", Tumes pagasts, Tukuma novads</p>

                    <p><b>Tālrunis​:</b> (+371) 29803510<br>
                    <b>E-pasts:</b> vilomix@vilomix.lv</p>

                </div>
                <div class="col-12 col-md-4 footer-col">
                    <?php $home_url = get_home_url(); ?>
                    <a href="<?php echo $home_url; ?>/precu-pardosanas-noteikumi/">Preču pārdošanas noteikumi juridiskām personām</a></br>
                    <a href="<?php echo $home_url; ?>/precu-pardosanas-noteikumi-pateretajiem/">Preču pārdošanas noteikumi patērētājiem</a></br>
                    <a href="<?php echo $home_url; ?>/privacy-policy/">Privātuma - sīkdatņu politika</a></br>
                    <a href="<?php echo $home_url; ?>/par-vlomix/">Par mums</a></br>
                    <a href="<?php echo $home_url; ?>/piegade/">Piegāde</a></br>
                    <a href="<?php echo $home_url; ?>/atlaizu-politika">Atlaižu politika</a>
                </div>
                <div class="col-12 col-md-4 footer-col">
                    <h3>SOCIĀLO TĪKLU SAITES</h3>
                    <div class="social-icon facebook"><a href="https://www.facebook.com/VilomixBaltic"></a></div>
                </div>
            </div>
			

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

<?php

add_action("wp_ajax_update_cart_page", "update_cart_page");
add_action("wp_ajax_nopriv_update_cart_page", "update_cart_page");

function update_cart_page(){

    $product_id = $_POST['id'];
    $new_amount = intval($_POST['new_value']);
    
    $cart = WC()->cart;
    $prev_weight = WC()->cart->get_cart_contents_weight();
    $product_cart_id = WC()->cart->generate_cart_id( $product_id );
    $cart_item_key = WC()->cart->find_product_in_cart( $product_cart_id );
    $prev_price = WC()->cart->get_totals()['subtotal'];

    if ($new_amount) {
        WC()->cart->set_quantity( $cart_item_key, $new_amount, true );
    } else {
        WC()->cart->remove_cart_item( $cart_item_key );
    }

    $price = WC()->cart->get_totals()['subtotal'];
    $weight = WC()->cart->get_cart_contents_weight();
    
    set_shipping_method();

    $price = WC()->cart->get_totals()['subtotal'];

    ob_start();

    show_cart_page();
    $data = ob_get_contents();

    ob_end_clean();

    ob_start();

    show_cross_sells();
    $cross_sells = ob_get_contents();

    ob_end_clean();

    $response = array(
        "cart" => show_quick_cart(),
        "cart_page" => $data,
        "cross_sells" => $cross_sells,
        "in_cart" => WC()->cart->cart_contents_count,
        "cart_weight" => WC()->cart->get_cart_contents_weight(),
        "prev_weight" => $prev_weight,
        "prev_price" => $prev_price,
        "price" => $price
    );
    
    echo json_encode($response);
    
    wp_die();

}
<?php

add_action("wp_ajax_remove_coupons", "remove_coupons");
add_action("wp_ajax_nopriv_remove_coupons", "remove_coupons");

function remove_coupons(){

    foreach ( WC()->cart->get_coupons() as $code => $coupon ){
        WC()->cart->remove_coupon( $code );
    }

    WC()->cart->persistent_cart_update();
    global $woocommerce;
    var_dump($woocommerce->cart->get_total_discount());

    ob_start();

    show_cart_page();
    $data = ob_get_contents();

    ob_end_clean();

    ob_start();

    show_cross_sells();
    $cross_sells = ob_get_contents();

    ob_end_clean();

    $response = array(
        "cart" => show_quick_cart(),
        "cart_page" => $data,
        "cross_sells" => $cross_sells
    );
    
    echo json_encode($response);
    
    wp_die();

}
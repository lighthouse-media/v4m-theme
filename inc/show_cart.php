<?php 
function show_cart_page() {
    /*if (!intval(WC()->cart->get_totals()['subtotal'])) {
        return false;
    }*/
?>
    <div style="display:none"class="qwer">
        <?php
            var_dump(WC()->cart->get_cart());
        ?>
    </div>
    <form class="woocommerce-cart-form row" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
        <div class="col-12 col-lg-9">
            <?php do_action( 'woocommerce_before_cart_table' ); ?>

            <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents " cellspacing="0">
                <thead>
                    <tr>
                        <th class="product-thumbnail">Produkts</th>
                        <th class="product-name"></th>
                        <th class="product-price"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
                        <th class="product-quantity">Daudzums (gab)</th>
                        <th class="product-weight">Svars</th>
                        <th class="product-subtotal">Kopā</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php do_action( 'woocommerce_before_cart_contents' ); ?>

                    <?php
                    $total_neto = 0;
                    $product_original_price = 0;

                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                        $product_original_price += $_product->get_regular_price() * $cart_item['quantity'];
                        

                        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                            ?>
                            <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">


                                <td class="product-thumbnail">
                                <?php
                                $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                                if ( ! $product_permalink ) {
                                    echo $thumbnail; // PHPCS: XSS ok.
                                } else {
                                    printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
                                }
                                ?>
                                </td>

                                <td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
                                <?php
                                if ( ! $product_permalink ) {
                                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                                } else {
                                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                                }

                                do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

                                // Meta data.
                                echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

                                // Backorder notification.
                                if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                    echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
                                }
                                ?>
                                </td>

                                <td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
                                    <?php
                                        echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                    ?>
                                </td>

                                <td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
                                    <div class="amount-controls" data-id="<?= $product_id; ?>" data-max="<?= $_product->get_max_purchase_quantity(); ?>" data-value="<?= $cart_item['quantity']; ?>">
                                        <div class="ctrl-btn" data-value="-1">-</div>
                                        <div class="amount"><input type="number" value="<?= $cart_item['quantity']; ?>"></div>
                                        <div class="ctrl-btn" data-value="1">+</div>
                                    </div>
                                </td>

                                <td class="product-weight" data-title="<?php esc_attr_e( 'Svars', 'woocommerce' );?>">
                                    <?php 
                                        $items_in_basket = $cart_item['quantity'];
                                        $item_weight = $_product->get_weight();
                                        echo $items_in_basket * $item_weight . 'kg';
                                    ?>
                                </td>

                                <td class="product-subtotal" data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>">
                                    <?php
                                        echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                    ?>
                                </td>
                                <td class="remve-product"><div class="remove" data-id="<?= $product_id; ?>"></div></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    <?php do_action( 'woocommerce_cart_contents' ); ?>

                    <tr>
                        <td colspan="5" class="back">
                                <?php if ( wc_coupons_enabled() ) { ?>
                                    <div class="coupon">
                                        <label for="coupon_code"><?php esc_html_e( 'MAN IR ATLAIDES KODS:', 'woocommerce' ); ?></label><br>
                                        <div class="coupon-row">
                                            <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
                                            <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
                                            <?php do_action( 'woocommerce_cart_coupon' ); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            
                        </td>
                        <td colspan="2" class="actions">
                            <div class="action-wrapper">
                                <div class="btn">Turpināt iepirkties<a href="<?= get_home_url() ; ?>"></a></div>
                            </div>

                            <!-- <button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>">Atjanunot</button>

                            <?php do_action( 'woocommerce_cart_actions' ); ?>

                            <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?> -->
                        </td>
                    </tr>

                    <?php do_action( 'woocommerce_after_cart_contents' ); ?>
                </tbody>
            </table>
            <?php do_action( 'woocommerce_after_cart_table' ); ?>
            
        </div>
        <div class="col-12 col-lg-3 cart-totals">
            <?php
            WC()->cart->persistent_cart_update();
                $cart = WC()->cart;
                $totals = $cart->get_totals();
                //var_dump($totals);
                $totals_before_coupon = $totals["subtotal"];
                $total_used_coupons = $totals["discount_total"];
                $total_price = $totals['total'];
                $subtotal = $totals['subtotal'];
                $no_pvn = $subtotal/1.21;
                $pvn = $subtotal - $no_pvn;
                //var_dump(new WC_Order());
            ?>
            <div class="totals-wrapper">
                <div class="total-line">
                    <div class="total-title">Summa bez PVN:</div>
                    <div class="total-value">€<?= number_format($no_pvn, 2, ',', '.'); ?></div>
                </div>
                <div class="total-line">
                    <div class="total-title">PVN 21%:</div>
                    <div class="total-value">€<?= number_format($pvn, 2, ',', '.'); ?></div>
                </div>
                <?php
                    if ($total_used_coupons) {
                        echo "<div class=\"total-line\">
                            <div class=\"total-title\">Izmantotas atlaides:</div>
                            <div class=\"total-value\">€". number_format($total_used_coupons, 2, ',', '.') . "</div>
                        </div>";
                        echo "<div class=\"remove-coupons btn\">Noņemt kuponu</div>";
                    }
                ?>
                <div class="shipping">

                        <?php
                            function bbloomer_get_all_shipping_zones() {
                                $data_store = WC_Data_Store::load( 'shipping-zone' );
                                $raw_zones = $data_store->get_zones();
                                foreach ( $raw_zones as $raw_zone ) {
                                   $zones[] = new WC_Shipping_Zone( $raw_zone );
                                }
                                $zones[] = new WC_Shipping_Zone( 0 ); // ADD ZONE "0" MANUALLY
                                return $zones;
                            }

                            function bbloomer_get_all_shipping_rates() {
                                foreach ( bbloomer_get_all_shipping_zones() as $zone ) {
                                   $zone_shipping_methods = $zone->get_shipping_methods();
                                   foreach ( $zone_shipping_methods as $index => $method ) {
                                      $method_is_taxable = $method->is_taxable();
                                      $method_is_enabled = $method->is_enabled();
                                      $method_instance_id = $method->get_instance_id();
                                      $method_title = $method->get_method_title(); // e.g. "Flat Rate"
                                      $method_description = $method->get_method_description();
                                      $method_user_title = $method->get_title(); // e.g. whatever you renamed "Flat Rate" into
                                      $method_rate_id = $method->get_rate_id(); // e.g. "flat_rate:18"
                                   }
                                   print_r( $zone_shipping_methods );  
                                   echo '<br><br><br><br><br>';
                                }
                             }

                             $packages = WC()->shipping->get_packages();
                             //var_dump($packages);
                             //bbloomer_get_all_shipping_rates();
                        ?>

                             

                        <?php /* do_action( 'woocommerce_cart_totals_before_shipping' ); */ ?>

                        <?php wc_cart_totals_shipping_html(); ?>

                        <?php /* do_action( 'woocommerce_cart_totals_after_shipping' ); */ ?>


                </div>

                <?php
                    $totals = $cart->get_totals();
                    $discount_totals = $totals['discount_total'];

                    $product_discounts = number_format($product_original_price - floatval($totals['subtotal']), 2);
                    
                    
                    $selected_shipping = WC()->cart->calculate_shipping();
                    $shipping_cost = floatval($selected_shipping[0]->get_cost());
                    $shipping_discount = $shipping_cost ? 0 : 3.99;
                ?>

                <div class="total-line">
                    <div class="total-title">Kopējais svars (neto):</div>
                    <div class="total-value"><?= number_format($cart->get_cart_contents_weight(), 2, ',', '.'); ?>kg</div>
                </div>

                <div class="total-line last-total-line">
                    <div class="total-title">Kopējā summa:</div>
                    <div class="total-value">
                        
                        €<?= number_format($total_price, 2, ',', '.'); ?>
                    </div>
                </div>

                <?php if ($product_discounts || $discount_totals || $shipping_discount) : ?>
                    <div class="discounts">
                        <?php 
                            if ( $product_discounts && $product_discounts !== '0.00' ) echo '<div class="disc-row"><span>Akciju atlaides:</span> <span>€' . number_format($product_discounts, 2, ',', '.') . '</span></div>';
                            if ( $discount_totals ) { echo '<div class="disc-row"><span>Kuponu atlaides:</span> <span>€' . number_format($discount_totals, 2, ',', '.') . '</span></div>'; }
                            if ( $shipping_discount ) echo '<div class="disc-row"><span>Piegādes atlaides:</span> <span>€' . number_format($shipping_discount, 2, ',', '.') . '</span></div>';
                            if ( $totals['fee_total'] ) echo '<div class="disc-row"><span>Apjoma atlaides:</span> <span>€' . number_format($totals['fee_total']*-1, 2, ',', '.') . '</span></div>';
                        ?>
                    </div>
                <?php endif; ?>
                

                
                

                
            </div>
            <?php
                $weight = WC()->cart->get_cart_contents_weight();
                if ($weight > 500) 
                    echo '<p class="over-500">Par sūtījumiem virs 500 kg lūdzam sazināties ar mums izdevīgākai cenai. Kontakttālrunis +371 29803510 (darba dienās no 8.00-16.00)</p>';
            ?>
            <div class="to-checkout-wrapper">
                <div class="to-checkout">Uz apmaksu<a href="<?= wc_get_checkout_url(); ?>"></a></div>
            </div>
        </div>
    </form>
<?php } ?>
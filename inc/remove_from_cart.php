<?php

add_action("wp_ajax_remove_from_cart", "remove_from_cart");
add_action("wp_ajax_nopriv_remove_from_cart", "remove_from_cart");

function remove_from_cart(){

    $product_id = $_POST['id'];
    
    $cart = WC()->cart;
    $product_cart_id = $cart->generate_cart_id( $product_id );
    $cart_item_key = $cart->find_product_in_cart( $product_cart_id );

    $cart->remove_cart_item( $cart_item_key );

    $price = WC()->cart->get_totals()['subtotal'];
    $weight = WC()->cart->get_cart_contents_weight();
    
    set_shipping_method();
/* 
    ob_start();

    show_cart_page();
    $data = ob_get_contents();

    ob_end_clean();

    ob_start();

    show_cross_sells();
    $cross_sells = ob_get_contents();

    ob_end_clean(); */

    $response = array(
        "cart" => show_quick_cart(),
        /*"cart_page" => $data ? $data : false,
        "cross_sells" => $cross_sells,*/
        "in_cart" => WC()->cart->cart_contents_count,
        "cart_weight" => WC()->cart->get_cart_contents_weight()
    );
    
    echo json_encode($response);
    
    wp_die();

}
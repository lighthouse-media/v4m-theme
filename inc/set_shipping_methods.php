<?php

function set_shipping_method() {
    $selected_shipping = WC()->session->get('chosen_shipping_methods' );
    $weight = WC()->cart->get_cart_contents_weight();

    if ( $selected_shipping[0] !== 'omnivalt_pt' && $selected_shipping[0] !== 'free_shipping:7' ) {
        if ($weight >= 100) {
            WC()->session->set('chosen_shipping_methods', array( 'free_shipping:1' ) );
        } else if(check_if_free_delivery_coupon_applied()) {
            WC()->session->set('chosen_shipping_methods', array( 'free_shipping:4' ) );
        } else {
            WC()->session->set('chosen_shipping_methods', array( 'flat_rate:2' ) );
        }
    }
}

?>
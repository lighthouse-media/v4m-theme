<?php 

function my_admin_menu() {
    add_menu_page(
        __( 'Newsletter subscribers', 'my-textdomain' ),
        __( 'Newsletter subscribers', 'my-textdomain' ),
        'manage_options',
        'newsletter-subscribers',
        'my_admin_page_contents',
        'dashicons-schedule',
        3
    );
}

add_action( 'admin_menu', 'my_admin_menu' );


function my_admin_page_contents() {
    $args = array(
        'post_type' => 'shop_order',
        'posts_per_page' => '-1',
        'post_status' => array('wc-cancelled', 'wc-failed', 'wc-on-hold', 'wc-processing', 'wc-completed', 'wc-failed') 
    );

    $orders = query_posts($args);
    $index = 1;
    $unique_mails = [];

    foreach( $orders as $order ) {
        $subs = get_post_meta($order->ID, '_billing_wooccm14', true);
        $mail = get_post_meta($order->ID, '_billing_email', true);

        if ($subs == 'Yes') {
            
            if ( array_search($mail, $unique_mails) == false ) {
                array_push($unique_mails, $mail);
            }
            
        }
        
    }


    echo '<h1>Subscribed to newsletter</h1>';
    echo '<table>';
        foreach($unique_mails as $the_mail) {
            echo '<tr><td>' . $index . '. </td><td>'. $the_mail .'</td></tr>';
            $index++;
        }
    echo '</table>';
}
<?php


add_action("wp_ajax_get_quick_cart", "get_quick_cart");
add_action("wp_ajax_nopriv_get_quick_cart", "get_quick_cart");

function get_quick_cart() {
    echo show_quick_cart();
    wp_die();
}


function show_quick_cart() {
    $cart = WC()->cart;
    $total_price = $cart->get_totals()['cart_contents_total'];
    $the_cart = WC()->cart->get_cart();
    $total = $cart->get_totals();

    $total_weight = WC()->cart->get_cart_contents_weight();
    $show_weight_disclaimer =  $total_weight >= 50 && 100 > $total_weight;

    $go_up_to = 0;
    $up_to_val = 0;
    /*if ( $total['subtotal'] >= 400 && $total['subtotal'] < 500 ) {
        $go_up_to = 500; $up_to_val = 5;
    } elseif ( $total['subtotal'] >= 900 && $total['subtotal'] < 1000 ) {
        $go_up_to = 1000; $up_to_val = 10;
    } *//* elseif ( $total['subtotal'] >= 1400 && $total['subtotal'] < 1500 ) {
        $go_up_to = 1500; $up_to_val = 15;
    } elseif ( $total['subtotal'] >= 1900 && $total['subtotal'] < 2000 ) {
        $go_up_to = 2000; $up_to_val = 20;
    } */

    $total_amount = 0;

    $item_table = '<div class="quick-cart-modal"><table>';

    foreach($the_cart as $cart_item_key => $cart_item) {
        $total_amount += $cart_item["quantity"];

        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
        //var_dump($cart_item);
        $item_table .= sprintf('<tr><td>%s</td><td>%s</td><td><div class="remove-item-button" data-id="%s"></div></td></tr>',
            $_product->get_name(),
            apply_filters( 'woocommerce_cart_item_subtotal', $cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ),
            $product_id
        );
    }


    
    $item_table .= '</table>';

    if ( count($the_cart) ) {
        $item_table .= '<div class="go-to-cart-wrapper"><div class="go-to-cart"><span>NOFORMĒT PIRKUMU</span><a href="' . wc_get_cart_url() . '"></a></div></div>';
    }

    $item_table .= '</div>';

    $cart_url = wc_get_cart_url();
    
    $return_html='';

    if ($show_weight_disclaimer || $go_up_to ) {
        $return_html = '<div class="weight-dissclaimer"><div class="weight-disclaimer-img"></div>';
        $return_html .= $show_weight_disclaimer ? '<div class="weight-disclaimer-txt">Palielinot groza apjomu līdz 100kg, saņemiet bezmaksas piegādi!</div>' : '';
        // $return_html .= $go_up_to ? '<div class="weight-disclaimer-txt">Pasūti preci par kopējo summu vismaz ' . $go_up_to . ' EUR (t.sk. PVN) un saņem ' . $up_to_val . '% apjoma atlaidi precēm, kurām pirkuma grozā ir norādīta regulārā cena!</div>' : '';
        $return_html .= '</div>';
    }

    $return_html .= "
        <div class=\"cart\">
            <div class=\"cart-icon\"></div>
            <div class=\"cart-description\">
                <p>Pirkuma grozs</p>
                <p>$total_amount preces, €$total_price</p>
            </div>
            <a href=\"$cart_url\"></a>
            $item_table
        </div>";

    return $return_html;
}


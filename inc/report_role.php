<?php
$caps = array(
    "view_woocommerce_reports" => true,
    "level_0" => true,
    "level_1" => true,
    "level_2" => true,
    "read" => true,
    "upload_files" => true,
    "edit_posts"=> true,

);

add_role( 'report', 'Woocommerce Reports', $caps );


function wpdocs_remove_menus(){
    
    $current_user = wp_get_current_user();
    
    if ( $current_user->roles[0] == 'report' ) {
        remove_menu_page( 'index.php' );                  //Dashboard
        remove_menu_page( 'jetpack' );                    //Jetpack* 
        remove_menu_page( 'edit.php' );                   //Posts
        remove_menu_page( 'upload.php' );                 //Media
        remove_menu_page( 'edit.php?post_type=page' );    //Pages
        remove_menu_page( 'edit.php?post_type=slides' );    //Pages
        remove_menu_page( 'admin.php?page=wpcf7' );
        remove_menu_page( 'admin.php?page=wc-order-export' );
        remove_menu_page( 'admin.php?page=wc-admin&path=%2Fmarketing' );
        remove_menu_page( 'edit-comments.php' );          //Comments
        remove_menu_page( 'themes.php' );                 //Appearance
        remove_menu_page( 'plugins.php' );                //Plugins
        remove_menu_page( 'users.php' );                  //Users
        remove_menu_page( 'tools.php' );                  //Tools
        remove_menu_page( 'options-general.php' );        //Settings
        remove_menu_page( 'admin.php?page=wc-admin');
        
        echo '
        <style>
            #menu-posts-product{display:none;}
            #toplevel_page_woocommerce{display:none;}
            #toplevel_page_woocommerce-marketing{display:none;}
            #toplevel_page_wc-order-export{display:none;}
            #toplevel_page_wpcf7{display: none;}
            #menu-users{display:none;}
            #wp-admin-bar-new-content{display:none;}
            #wp-admin-bar-comments{display:none;}
        </style>';
        
        echo '<script>
            const path = window.location.pathname;
            const href = window.location.href;
            console.log(path, href);
            console.log(!href.includes("page=wc-reports"));
            console.log(!href.includes("page=wc-admin&path=%2Fanalytics"));
            console.log(path !== "/wp-admin/");
            if (
                !href.includes("page=wc-reports") &&
                !href.includes("page=wc-admin&path=/analytics") &&
                path !== "/wp-admin/"
            ) {
                window.location.href = "' . get_admin_url() . '";
            }
            
            const listItems = document.querySelectorAll("li");
            
            console.log(listItems);
            
            /*$("#toplevel_page_wc-admin-path--analytics-overview li").each(function(){
                if($(this).text().includes("taxes")) $(this).hide();
            })*/
        </script>';
    }
}
add_action( 'admin_menu', 'wpdocs_remove_menus' );
<?php 

function show_cross_sells() {
    $cart = WC()->cart;
    $upsells = $cart -> get_cross_sells();
    if ($upsells && count($upsells) ) {
        echo '<h2>Saistītie produkti</h2>';
        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'post__in' => $upsells
        );
        $up_posts = new WP_Query($args);

        if ($up_posts->have_posts()) :
            echo '<div class="row cat-items">';
            while ($up_posts->have_posts() ) :
                $up_posts->the_post();
                wc_get_template_part( 'content', 'product' );
            endwhile;
            echo '</div>';
        endif;
        wp_reset_query();
    }
}
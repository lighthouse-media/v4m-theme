<?php

add_action('woocommerce_cart_calculate_fees', 'apply_discount_for_two_products_in_basket');

function apply_discount_for_two_products_in_basket() {

    $products = array( 132, 135, 138 );
    $total_items = 0;
    $total_price = 0;

    $cart_items = WC()->cart->get_cart();

    // Check if both products are in the cart
    foreach($cart_items as $cart_item_key => $cart_item){
        if ( in_array( $cart_item['product_id'], $products ) ) {
            $total_items += $cart_item['quantity'];
            $total_price += $cart_item["line_total"];
        }
    }

    if ( $total_items >= 2) { // Apply the discount if both products are in the cart
        $discount_perc = 0.30; // Replace with the discount amount
        $discount = $discount_perc * $total_price;
        WC()->cart->add_fee('Ņau Ņau Akcija', -$discount);
    }
}
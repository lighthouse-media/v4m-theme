<?php

function check_if_free_delivery_coupon_applied() {
    $free_delivery = false;
    
    $applied_coupons = WC()->cart->get_applied_coupons();

    foreach( $applied_coupons as $coupon_code ){
    
        $coupon = new WC_Coupon($coupon_code);
    
        if($coupon->get_free_shipping()){
            $free_delivery = true;
        }
    }
    
    return $free_delivery;
}
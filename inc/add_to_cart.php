<?php

add_action("wp_ajax_add_to_cart", "add_to_cart");
add_action("wp_ajax_nopriv_add_to_cart", "add_to_cart");

function add_to_cart(){

    $id = intval($_REQUEST['id']);
    $amount = intval($_REQUEST['amount']);
    $prev_weight = WC()->cart->get_cart_contents_weight();
    $prev_price = WC()->cart->get_totals()['subtotal'];
    $res = WC()->cart->add_to_cart( $id, $amount );

    if ( !$res ) { echo 'error'; return wp_die(); }
    $price = WC()->cart->get_totals()['subtotal'];
    $weight = WC()->cart->get_cart_contents_weight();
    
    set_shipping_method();
    
    $product = wc_get_product( $id );
    $item_quantities = WC()->cart->get_cart_item_quantities();
    $stock = intval(get_post_meta( $id, '_stock', true )) - $item_quantities[$id];
    
    $response = array(
        "title" => $product->get_name(),
        "img" => $product->get_image(),
        "amount" => $item_quantities[$id],
        "cart" => show_quick_cart(),
        "in_cart" => WC()->cart->cart_contents_count,
        "cart_weight" => WC()->cart->get_cart_contents_weight(),
        "prev_weight" => $prev_weight,
        "max_leftover" => $stock,
        "prev_price" => $prev_price,
        "price" => $price
    );

    $response['cart_info'] = sprintf('<div class="added-to-cart-info">
    <div class="close-btn"></div>
    <div class="panel-info">
        <div class="panel-icon"></div>
        <h2>Prece pievienota grozam</h2>
        <div class="prod-info">
            <div class="prod-img"> %s </div>
            <div class="prod-name">%s</div>
        </div>
        <div class="button-block">
            <div class="btn close-btn">Turpināt iepirkties</div>
            <div class="buy btn">
                Pirkt
                <a href="%s"></a>
            </div>
        </div>
    </div>
</div>',
        $product->get_image(), 
        $product->get_name(),
        wc_get_cart_url()
    );
    
    //var_dump($response);
    
    echo json_encode($response);
    
    wp_die();

}
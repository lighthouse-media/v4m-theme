<!-- Global site tag (gtag.js) - Google Analytics -->
<?php

    if ($_COOKIE['cookielawinfo-checkbox-analytics'] && $_COOKIE['cookielawinfo-checkbox-analytics'] == 'yes') { 

?>

    <script async src="https://www.googletagmanager.com/gtag/js?id=G-4SB1CQ2S6W"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-4SB1CQ2S6W');
    </script>

<?php
     } else  {
        unset($_COOKIE['_ga']);
        unset($_COOKIE['_ga_4SB1CQ2S6W']);
    }
?>
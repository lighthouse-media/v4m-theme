<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    //wp_enqueue_style( 'child-style', get_stylesheet_uri(), wp_get_theme()->get('Version')  );
    wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css', wp_get_theme()->get('Version')  );
    
    wp_enqueue_script( 'vilomix-scripts', get_stylesheet_directory_uri() . '/js/scripts.js',  array('jquery'), wp_get_theme()->get('Version') );
    wp_enqueue_script( 'bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js',  wp_get_theme()->get('Version') );

    wp_localize_script( 'vilomix-scripts', 'ajaxLocal', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	));
}

//ajax 
require get_stylesheet_directory() . '/inc/add_to_cart.php';

// show_cart()
require get_stylesheet_directory() . '/inc/show_cart.php';

// supdate_cart_page()
require get_stylesheet_directory() . '/inc/update_cart.php';

// remove_from_cart()
require get_stylesheet_directory() . '/inc/remove_from_cart.php';

// remove_coupons()
require get_stylesheet_directory() . '/inc/remove_coupons.php';

require get_stylesheet_directory() . '/inc/show_quick_cart.php';

// supdate_cart_page()
require get_stylesheet_directory() . '/inc/cart_cross_sells.php';

require get_stylesheet_directory() . '/inc/omniva_class_update.php';

require get_stylesheet_directory() . '/inc/admin_subscribers.php';

//Add report role and setup it
require get_stylesheet_directory() . '/inc/report_role.php';

//Check if free delivery coupon applied
require get_stylesheet_directory() . '/inc/check_if_free_delivery.php';

//function that determines shipping method and sets it
require get_stylesheet_directory() . '/inc/set_shipping_methods.php';

// hook removal
add_action( 'init', 'remove_storefront_actions');
function remove_storefront_actions() {

    remove_action( 'storefront_footer', 'storefront_credit', 20);
}


if ( ! function_exists( 'vilomix_register_nav_menu' ) ) {
 
    function vilomix_register_nav_menu(){
        register_nav_menus( array(
            'info_menu' => 'Info menu',
            'container' => ''
        ) );
    }
    add_action( 'after_setup_theme', 'vilomix_register_nav_menu', 0 );
}


// Hide product category textarea

function wpse_hide_cat_descr() { ?>

    <style type="text/css">
       .term-description-wrap {
           display: none;
       }
    </style>

<?php } 

add_action( 'admin_head-term.php', 'wpse_hide_cat_descr' );
add_action( 'admin_head-edit-tags.php', 'wpse_hide_cat_descr' );


function vilomix_show_categories( $category=0 ){

    $args = array(
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'parent' => $category,
        'exclude' => [15]
	);

	$next = get_terms($args);

    $next = get_terms('product_cat', $args);

    $r = '';

    if ($next) {
        $r .= '<ul>';

        foreach ($next as $cat) {
            $r .= '<li><a href="' . get_term_link($cat->slug, $cat->taxonomy) . '" title="' . sprintf(__("View all products in %s"), $cat->name) . '" ' . '>' . $cat->name . ' (' . $cat->count . ')' . '</a>';
            $r .= $cat->term_id !== 0 ? vilomix_show_categories($cat->term_id) : null;
        }
        $r .= '</li>';

        $r .= '</ul>';
    }

    return $r;
}

function get_display_unit( $product, $parent = null ) {
    $display_unit = $unit = $product->get_attribute( 'unit' );
    if ($parent) {
        $measurement_unit = get_post_meta($parent->get_id(), 'unit_measurement', true);
    } else {
        $measurement_unit = get_post_meta($product->get_id(), 'unit_measurement', true);
    }
    
    if ($measurement_unit == 'kg') {
        if (floatval($unit) < 1) { 
            $display_unit = '' . floatval($unit) * 1000 . ' g';
        } else { 
            $display_unit .= ' kg';
        }
    }
    return $display_unit;
}

function show_stock($stock) {
    if ($stock >= 10) return 'Ir noliktavā';
    if ($stock < 10 && $stock > 0 ) return 'Noliktavā ' . $stock;
    if (!$stock) return 'Nav noliktavā';
}


/*****************ENALE SVG UPLOAD *******************/

add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

    global $wp_version;
    if ( $wp_version !== '4.7.1' ) {
       return $data;
    }
  
    $filetype = wp_check_filetype( $filename, $mimes );
  
    return [
        'ext'             => $filetype['ext'],
        'type'            => $filetype['type'],
        'proper_filename' => $data['proper_filename']
    ];
  
  }, 10, 4 );
  
  function cc_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter( 'upload_mimes', 'cc_mime_types' );
  
  function fix_svg() {
    echo '<style type="text/css">
          .attachment-266x266, .thumbnail img {
               width: 100% !important;
               height: auto !important;
          }
          </style>';
  }
  add_action( 'admin_head', 'fix_svg' );

//add_filter( 'load_textdomain_mofile', 'load_custom_plugin_translation_file', 10, 2 );

/*
 * Replace 'textdomain' with your plugin's textdomain. e.g. 'woocommerce'. 
 * File to be named, for example, yourtranslationfile-en_GB.mo
 * File to be placed, for example, wp-content/lanaguages/textdomain/yourtranslationfile-en_GB.mo
 */
/* function load_custom_plugin_translation_file( $mofile, $domain ) {
    $mofile = WP_LANG_DIR . '/woocommerce/woocommerce-lv_LV.mo';
  return $mofile;
} */


function custom_posts_per_page( $query ) {

    if ( isset($query->query_vars['wc_query']) ) {
        
      $posts_per_page = 120;
  
      set_query_var('posts_per_page', $posts_per_page);
  
    }
  }
  add_action( 'pre_get_posts', 'custom_posts_per_page' );



  function list_hooks( $hook = '' ) {
    global $wp_filter;

    if ( isset( $wp_filter[$hook]->callbacks ) ) {      
        array_walk( $wp_filter[$hook]->callbacks, function( $callbacks, $priority ) use ( &$hooks ) {           
            foreach ( $callbacks as $id => $callback )
                $hooks[] = array_merge( [ 'id' => $id, 'priority' => $priority ], $callback );
        });         
    } else {
        return [];
    }

    foreach( $hooks as &$item ) {
        // skip if callback does not exist
        if ( !is_callable( $item['function'] ) ) continue;

        // function name as string or static class method eg. 'Foo::Bar'
        if ( is_string( $item['function'] ) ) {
            $ref = strpos( $item['function'], '::' ) ? new ReflectionClass( strstr( $item['function'], '::', true ) ) : new ReflectionFunction( $item['function'] );
            $item['file'] = $ref->getFileName();
            $item['line'] = get_class( $ref ) == 'ReflectionFunction' 
                ? $ref->getStartLine() 
                : $ref->getMethod( substr( $item['function'], strpos( $item['function'], '::' ) + 2 ) )->getStartLine();

        // array( object, method ), array( string object, method ), array( string object, string 'parent::method' )
        } elseif ( is_array( $item['function'] ) ) {

            $ref = new ReflectionClass( $item['function'][0] );

            // $item['function'][0] is a reference to existing object
            $item['function'] = array(
                is_object( $item['function'][0] ) ? get_class( $item['function'][0] ) : $item['function'][0],
                $item['function'][1]
            );
            $item['file'] = $ref->getFileName();
            $item['line'] = strpos( $item['function'][1], '::' )
                ? $ref->getParentClass()->getMethod( substr( $item['function'][1], strpos( $item['function'][1], '::' ) + 2 ) )->getStartLine()
                : $ref->getMethod( $item['function'][1] )->getStartLine();

        // closures
        } elseif ( is_callable( $item['function'] ) ) {     
            $ref = new ReflectionFunction( $item['function'] );         
            $item['function'] = get_class( $item['function'] );
            $item['file'] = $ref->getFileName();
            $item['line'] = $ref->getStartLine();

        }       
    }

    return $hooks;
}


//add_filter( 'woocommerce_package_rates', 'conditionally_hide_other_shipping_based_on_items_weight', 100, 1 );
function conditionally_hide_other_shipping_based_on_items_weight( $rates ) {
    
    $total_weight = WC()->cart->get_cart_contents_weight();
    $passed = $total_weight > 10;

    //var_dump($rates);

    $free = array();
    $not_free_rates = array();
    foreach ( $rates as $rate_id => $rate ) {
        if ( $passed ) {
            if ( 'free_shipping:1' === $rate_id) {
                $free[ $rate_id ] = $rate;
            } else {
                $not_free_rates[ $rate_id ] = $rate;
            }
        } else {
            if ( 'free_shipping:1' !== $rate_id) {
                $not_free_rates[ $rate_id ] = $rate;
            }
        }
    }

    return ! empty( $free ) ? $free : $not_free_rates;
}


add_filter( 'woocommerce_checkout_fields', 'remove_checkout_fields', 100 );
function remove_checkout_fields( $fields ) {

    $selected_shipping = WC()->session->get('chosen_shipping_methods' );
   

    if ($selected_shipping[0] == 'omnivalt_pt') {
        unset($fields['billing']['billing_address_1']);
        unset($fields['billing']['billing_address_2']);
        unset($fields['billing']['billing_city']);
        unset($fields['billing']['billing_postcode']);
        unset($fields['billing']['billing_country']);
        unset($fields['billing']['billing_state']);
        unset($fields['billing']['billing_wooccm17']);
    } else {
        unset($fields['billing']['billing_wooccm21']);
    }
    unset($fields['shipping']['shipping_state']);
    unset($fields['billing']['billing_state']);

    unset($fields['shipping']['shipping_country']);
    unset($fields['billing']['billing_country']);
    add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );
    

	return $fields;
}

add_filter('locale', 'wpse27056_setLocale');
function wpse27056_setLocale($locale) {

        return 'lv_LV';

}


add_filter( 'woocommerce_email_attachments', 'bbloomer_attach_pdf_to_emails', 10, 4 );
 
function bbloomer_attach_pdf_to_emails( $attachments, $email_id, $order, $email ) {
    $email_ids = array( 'new_order' );
    if ( in_array ( $email_id, $email_ids ) ) {

        $shipping_method = $order->get_shipping_method();
        if ( $shipping_method == 'Omniva pakomāts' || $shipping_method == 'Omniva parcel terminal' ) {
            $omnivalt_labels = new OmnivaLt_Labels_Update();
            $order_id = $order->get_id();
            $file_path = $omnivalt_labels->print_labels( $order_id, true);
            $attachments[] = $file_path;
        }

    }
    return $attachments;
}

function vm_amount_discounts( $cart_object ) {

    return; //this is disabled
    
    global $woocommerce;
    $total_fee = 0;
    $total_fee_val = 0;
    $total_discount = 0;
    $totals = WC()->cart->get_totals();
    
    if ($totals['subtotal']< 500) {
        return;  
    } elseif ( $totals['subtotal'] >= 500 && $totals['subtotal'] < 1000 ) {
        $total_discount = 0.05;
    } elseif ( $totals['subtotal'] >= 1000 /* && $totals['subtotal'] < 1500 */ ) {
        $total_discount = 0.1;
    } /* elseif ( $totals['subtotal'] >= 1500 && $totals['subtotal'] < 2000 ) {
        $total_discount = 0.15;
    } elseif ( $totals['subtotal'] >= 2000 ) {
        $total_discount = 0.2;
    }*/

    foreach ( $cart_object->cart_contents as $key => $value ) {
       $_product =  wc_get_product( $value['data']->get_id() );
       if ($_product->is_on_sale()) continue;
       $total_fee_val+= $_product->get_price() * $value['quantity'];
    }
    
    //var_dump($total_fee_val);
    
    $total_fee = $total_fee_val * $total_discount * -1;
    
    //var_dump($total_fee);
    
    $woocommerce->cart->add_fee( 'Apjoma atlaide', $total_fee, true, 'standard' );
    

}

add_action( 'woocommerce_cart_calculate_fees', 'vm_amount_discounts' );


/*******************DEBUG FUNCTION */

add_shortcode( 'activeplugins', function(){
	
	$active_plugins = get_option( 'active_plugins' );
	$plugins = "";
	if( count( $active_plugins ) > 0 ){
		$plugins = "<ul>";
		foreach ( $active_plugins as $plugin ) {
			$plugins .= "<li>" . $plugin . "</li>";
		}
		$plugins .= "</ul>";
	}
	return $plugins;
});


/*****************NAU NAU DISCOUNTS*******************/
require get_stylesheet_directory() . '/inc/temp/naunau_discounts.php';

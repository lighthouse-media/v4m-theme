<?php /* Template Name: About page */ ?>

<?php

get_header(); ?>

    <div class="container">

			<?php
			while ( have_posts() ) :
				the_post();

                $thumbnail = get_the_post_thumbnail_url( $post, 'full' );
                $the_content = get_the_content( $post->ID );
                $hide_header = get_post_meta( $post->ID, 'hide_header', true );
                if (!$hide_header)
                    echo sprintf('<h1>%s</h1>', get_the_title( $post ));
                echo sprintf('<div class="about-content-wrapper"><div class="about-main-img" style="background-image:url(%s)"></div><div class="about-text-content">%s</div></div>',
                    $thumbnail,
                    $the_content
            );
				
			endwhile; // End of the loop.
			?>


	</div><!-- #primary -->

<?php
get_footer();


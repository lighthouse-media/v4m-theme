<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<?php /* require get_stylesheet_directory() . '/inc/google_analytics.php'; */?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">
	<?php do_action( 'storefront_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="logo-wrapper col-2">
                        <img class="vilomix-logo" src="<?= get_stylesheet_directory_uri(); ?>/src/vilomix-w.svg">
                    </div>
                    <div class="col-4 welcome-txt">Laipni lūgti Vilomix interneta veikalā!</div>
                    <div class="col-6 cart-wrapper">
                        <div class="call-now">
                            <span class="phone-icon"></span>
                            <span class="phone-txt"><a href="tel:+37129803510">+371 29803510</a> (darba dienās 8.00-16.00)</span>
                        </div>
                        <div class="quick-cart-wrapper">
                            <?php echo show_quick_cart(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container second-line-wrapper">
            <div class="row header-main">
                <div class="col-6 logo"><img src="<?= get_stylesheet_directory_uri(); ?>/src/v4m_logo.png"><a href="<?php echo home_url();?>"></a></div>
                <!-- <div class="col-3 logo"><a href="<?php echo home_url();?>"><img src="<?= get_stylesheet_directory_uri(); ?>/src/vilomix.png"></a></div> -->
                <div class="col-6 nav-search">
                    <?php 

                        $menu_items_arr = wp_get_nav_menu_items(62);
                        $info_output = '';
                        if ( $menu_items_arr && count($menu_items_arr)) {
                            echo '<nav class="main-nav"><div class="nav-toggler">Informācija</div><div class="nav-dropdown"><ul>';
                                foreach( $menu_items_arr as $menu_item ) {
                                    $info_output .= '<li><a href="' . $menu_item->url . '">' . $menu_item->title . '</a></li>';
                                }
                                echo $info_output;
                            echo '</ul></div></nav>';
                        }
                    ?>
                    <nav class="main-nav">
                        <div class="nav-toggler">Produkti</div>
                        <div class="nav-toggler-mobile"></div>
                        <div class="nav-dropdown">
                            <?php echo vilomix_show_categories( 0 ); ?>
                            <?php
                                if ($info_output) {
                                    echo '<ul class="mob">' . $info_output . '</ul>';
                                }
                            ?>
                        </div>
                    </nav>
                    <div class="mobile-search-toggler"></div>
                    <div class="mobile-cart">
                        <?php
                            $count = WC()->cart->cart_contents_count;
                            $show_number_class = $count ? ' show': '';
                        ?>
                        <a href="<?= wc_get_cart_url();?>"></a>
                        <div class="mobile-cart-icon"></div>
                        <div class="items-in-cart<?= $show_number_class; ?>"><?= $count; ?></div>
                    </div>
                    <div class="search">
                        <?php storefront_product_search(); ?>
                    </div>
                    <?php
                        $total_weight = WC()->cart->get_cart_contents_weight();
                        $show_weight_disclaimer_class = $total_weight >= 50 && 100 > $total_weight ? ' show' : '';
                    ?>
                    <div class="mobile-weight-disclaimer<?= $show_weight_disclaimer_class; ?>">
                        
                        Palielinot groza apjomu līdz 100kg, saņemiet bezmaksas piegādi!
                    </div>
                </div>
            </div>
        </div>

	</header><!-- #masthead -->

    <div class="header-helper"></div>

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	do_action( 'storefront_before_content' );
	?>

	<div id="main-content" class="site-content" tabindex="-1">
		<div class="site-content">

		<?php
		do_action( 'storefront_content_top' );
